# Akilcab Starter Kit

Starter kit pour les applications AKILCAB généré avec [Angular CLI](https://github.com/angular/angular-cli) version 6.0.5.

## Démarrer

Pour lancer le serveur de développement, executer la commande `npm start` ou `yarn start`.
Ouvrir ensuite le navigateur à l'adresse `http://localhost:3200`. Vous pouver aussi lancer la commande `npm start -- -o` ou `yarn start -o` pour ouvrir automatiquement le navigateur la l'adresse du serveur.

Pour changer le port, rendez-vous dans le fichier `angular.json` à la ligne `97` et modifiez la valeur.

## Versionning

Pour le versionning, nous utilisons le package `superman-versioning`.

Pour mettre à jour une nouvelle version vous pouvez exécuter la commande `npm run build:version` ou `yarn build:version`.

Vous pouvez aussi utiliser la commande `superman version:update` pour une installation globale de `superman-versioning` ou juste la commande `npm run superman version:update` ou `yarn superman version:update` pour une installation locale.

## Theming

Des palettes de couleur pour les applications AKILCAB sont définies dans le fichier `src/theme/theme-colors.scss`.
Ces palettes ont été définies sur la base des couleurs du logo de chaque applications.

Pour remplacer les couleurs du template en fonction de l'application, consulter le fichier `src/theme/theme-variables.scss` et modifiez les lignes `9`, `10` en remplaçant les palettes par celle correspondantes.

Ex: Pour declarons, on aura

```scss
$primary: mat-palette($theme-declarons);
$accent: mat-palette($theme-declarons-light, 600, 400, 700);
```

## Logo

Les logos sont définis dans:

- `src/favicon.ico`
- `src/assets/icons`

Pour généré les icones au format définis dans le dossier `src/assets/icons`, utilisez ce lien [https://app-manifest.firebaseapp.com/](https://app-manifest.firebaseapp.com/).

## Arborescence

```
src/                            // Code source du projet
|- @theme/                      // Module du theme
|- app/                         // Module de l'application
|  |- @core/                    // Core de l'application (contient uniquement les services et le layout)
|  |- @shared/                  // Elements partagés dans l'application (contient les components, directives et pipes, ...)
|  |  |- components/            // Contient les components partagés dans toute l'application
|  |  |- dialogs/               // Contient toutes les fenêtres modales de l'application
|  |  |- modules/               // Contient des modules personnalisés partagés dans l'application
|  |  |- pipes/                 // Contient tous les pipes de l'application
|  |- main/                     // Contenu principal de l'application
|  |  |- cabinet/               // Contient l'interface cabinet de l'application
|  |  |- client/                // Contient l'interface client de l'application
|  |- app.component.*           // Component principal
|  |- app.module.ts             // Module principal
|  |- app-routing.module.ts     // Ensemble des routes de départ de l'application
|  |- app.settings.ts           // Paramétrage du layout de l'application
|  |- navigation-menu.ts        // Menus de l'application (cabinet, client)
|- assets/                      // Ensemble des iccones, images, fichiers internes
|- environments/                // Environnements de build
|- theme/                       // Theme prédéfini de l'application
|- translations                 // traductions statique du contenu
+- ...
angular.json                    // Configuration angular
```

## Traductions

Pour la traduction statique, définissez la directive `translate` dans la vue html de manière suivante: 

```html
<span translate>Texte à traduire</span>
<span>{{'Texte à traduire' | translate}}</span>
```

- Exécutez la commande `nmp run translations:extract` ou `yarn translations:extract` pour extraire les contenus a traduire dans le fichier `src/translations/template.json`.
- Copiez le contenu de `src/translations/template.json` dans `src/translations/en-Us.json` et `src/translations/fr-FR.json`.
- Traduisez manuellement chaque mot dans les fichiers `src/translations/fr-FR.json` ou `src/translations/en-Us.json`.

A noter que la langue par défaut est `fr-FR`.

Pour ajouter de nouvelle langue, créez un nouveau fichier correspondant dans `src/translations` et modifier les fichiers environnement dans `src/environments`.
