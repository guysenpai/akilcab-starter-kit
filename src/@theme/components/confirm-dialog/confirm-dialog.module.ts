import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material';

import { ConfirmDialogComponent } from '@theme/components/confirm-dialog/confirm-dialog.component';

@NgModule({
  imports: [MatDialogModule],
  exports: [ConfirmDialogComponent],
  declarations: [ConfirmDialogComponent],
  entryComponents: [ConfirmDialogComponent]
})
export class ConfirmDialogModule { }
