import {
  Component,
  OnInit,
  ViewEncapsulation,
  HostBinding,
  ContentChildren,
  QueryList,
  ElementRef,
  AfterContentInit,
  Renderer2 } from '@angular/core';

import { WidgetToggleDirective } from '@theme/components/widget/widget-toggle.directive';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WidgetComponent implements OnInit, AfterContentInit {

  @HostBinding('class.flipped') flipped = false;
  @ContentChildren(WidgetToggleDirective, {descendants: true}) toggleButtons: QueryList<WidgetToggleDirective>;

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    setTimeout(() => {
      this.toggleButtons.forEach((flipButton: WidgetToggleDirective) => {
        this.renderer.listen(flipButton.el.nativeElement, 'click', () => {
          this.toggle();
        });
      });
    });
  }

  toggle() {
    this.flipped = !this.flipped;
  }

}
