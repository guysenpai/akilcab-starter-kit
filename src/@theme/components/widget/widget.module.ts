import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WidgetComponent } from '@theme/components/widget/widget.component';
import { WidgetToggleDirective } from '@theme/components/widget/widget-toggle.directive';

@NgModule({
  imports: [CommonModule],
  exports: [WidgetComponent, WidgetToggleDirective],
  declarations: [WidgetComponent, WidgetToggleDirective]
})
export class WidgetModule { }
