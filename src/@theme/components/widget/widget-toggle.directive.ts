import { Directive, ElementRef, OnInit, AfterViewInit } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[widgetToggle]'
})
export class WidgetToggleDirective implements OnInit, AfterViewInit {

  constructor(public el: ElementRef) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

}
