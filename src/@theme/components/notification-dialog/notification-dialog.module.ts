import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material';

import { NotificationDialogComponent } from '@theme/components/notification-dialog/notification-dialog.component';

@NgModule({
  imports: [MatDialogModule],
  exports: [NotificationDialogComponent],
  declarations: [NotificationDialogComponent],
  entryComponents: [NotificationDialogComponent]
})
export class NotificationDialogModule { }
