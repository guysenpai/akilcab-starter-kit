import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-notification-dialog',
  templateUrl: './notification-dialog.component.html',
  styleUrls: ['./notification-dialog.component.scss']
})
export class NotificationDialogComponent implements OnInit {

  public notificationTitle: string;
  public notificationMessage: string;

  constructor(public dialogRef: MatDialogRef<NotificationDialogComponent>) { }

  ngOnInit() {
  }

}
