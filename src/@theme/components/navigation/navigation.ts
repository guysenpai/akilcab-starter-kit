export type Navigation = NavigationItem[];

export interface NavigationItem {
  id?: any;
  title: string;
  translate?: string;
  type: 'item' | 'collapse' | 'group';
  icon?: string|boolean;
  initialism?: string|boolean;
  badge?: NavigationItemBadge;
  children?: Array<NavigationItem>;
  url?: string;
  externalUrl?: boolean;
  exactMatch?: boolean;
  openInNewTab?: boolean;
  function?: () => void;
  hidden?: boolean;
}

export interface NavigationItemBadge {
  title: string|number;
  translate?: string;
  bg?: string;
  fg?: string;
}
