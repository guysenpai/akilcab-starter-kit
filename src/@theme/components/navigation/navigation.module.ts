import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material';
import { RouterModule } from '@angular/router';

import { NavigationComponent } from '@theme/components/navigation/navigation.component';
import { NavVerticalGroupComponent } from '@theme/components/navigation/vertical/nav-group/nav-vertical-group.component';
import { NavVerticalCollapseComponent } from '@theme/components/navigation/vertical/nav-collapse/nav-vertical-collapse.component';
import { NavVerticalItemComponent } from '@theme/components/navigation/vertical/nav-item/nav-vertical-item.component';
import { NavHorizontalCollapseComponent } from '@theme/components/navigation/horizontal/nav-collapse/nav-horizontal-collapse.component';
import { NavHorizontalItemComponent } from '@theme/components/navigation/horizontal/nav-item/nav-horizontal-item.component';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    RouterModule
  ],
  exports: [NavigationComponent],
  declarations: [
    NavigationComponent,
    NavVerticalGroupComponent,
    NavVerticalCollapseComponent,
    NavVerticalItemComponent,
    NavHorizontalCollapseComponent,
    NavHorizontalItemComponent
  ]
})
export class NavigationModule { }
