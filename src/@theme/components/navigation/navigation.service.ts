import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

import { Navigation, NavigationItem } from '@theme/components/navigation/navigation';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  flatNavigation: Navigation = [];
  onItemCollapsed = new Subject();
  onItemCollapseToggled = new Subject();

  get onNavigationChanged(): Observable<string> {
    return this._onNavigationChanged.asObservable();
  }

  get onNavigationRegistered(): Observable<any> {
    return this._onNavigationRegistered.asObservable();
  }

  get onNavigationUnregistered(): Observable<string> {
    return this._onNavigationUnregistered.asObservable();
  }

  private _currentNavigationKey: any = null;
  private _onNavigationChanged = new BehaviorSubject(null);
  private _onNavigationRegistered = new BehaviorSubject(null);
  private _onNavigationUnregistered = new BehaviorSubject(null);
  private _registry: {[key: string]: Navigation} = {};

  constructor() { }

  register(key: string, value: Navigation) {
    if (this._registry[key]) {
      console.error(`The navigation with the key "${key}" already exists. Either unregister it first or use a unique key.`);
    } else {
      this._registry[key] = value;
      this._onNavigationRegistered.next([key, value]);
    }
  }

  unregister(key: string) {
    if (!this._registry[key]) {
      console.warn(`The navigation with the key "${key}" doesn't exist in the registry.`);
    } else {
      delete this._registry[key];
      this._onNavigationUnregistered.next(key);
    }
  }

  getNavigation(key: string): Navigation {
    if (this._registry[key]) {
      return this._registry[key];
    }
    console.warn(`The navigation with the key "${key}" doesn't exist in the registry.`);
  }

  hasNavigation(key: string): boolean {
    if (this._registry[key]) {
      return true;
    }

    return false;
  }

  getFlatNavigation(navigationItems: Navigation) {
    for (let i = 0; i < navigationItems.length; i++) {
      const item = navigationItems[i];
      if (item.type !== 'item') {
        if (item.type === 'group' || item.type === 'collapse') {
          if (item.children) {
            this.getFlatNavigation(item.children);
          }
        }
      } else {
        this.flatNavigation.push({
          id: item.id || null,
          title: item.title || null,
          translate: item.translate || null,
          type: item.type,
          icon: item.icon || false,
          url: item.url || null,
          function: item.function || null,
          exactMatch: item.exactMatch || false,
          badge: item.badge || null,
        });
      }
    }
    return this.flatNavigation;
  }

  getCurrentNavigation(): Navigation {
    if (this._currentNavigationKey) {
      return this.getNavigation(this._currentNavigationKey);
    }
    console.warn(`The current navigation is not set.`);
  }

  setCurrentNavigation(key: string) {
    if (this._registry[key]) {
      this._currentNavigationKey = key;
      this._onNavigationChanged.next(key);
    } else {
      console.warn(`The navigation with the key "${key}" doesn't exist in the registry.`);
    }
  }

  getNavigationItem(id: string, navigation?: any): any {
    navigation = navigation || this.getCurrentNavigation();

    for (let i = 0; i < navigation.length; i++) {
      const item = navigation[i];
      if (item.id === id) {
        return item;
      }

      if (item.children) {
        const childItem = this.getNavigationItem(id, item.children);
        if (childItem) {
          return childItem;
        }
      }
    }

    return false;
  }

  getNavigationItemParent(id: string, navigation?: Navigation, parent?: any): any {
    navigation = navigation || this.getCurrentNavigation();
    parent = parent || navigation;

    for (let i = 0; i < navigation.length; i++) {
      const item = navigation[i];
      if (item.id === id) {
        return item;
      }

      if (item.children) {
        const childItem = this.getNavigationItemParent(id, item.children, item);
        if (childItem) {
          return childItem;
        }
      }
    }

    return false;
  }

  addNavigationItem(item: NavigationItem, id: string) {
    const navigation = this.getCurrentNavigation();
    if (id !== 'end') {
      if (id === 'start') {
        navigation.unshift(item);
      }
      const navItem = this.getNavigationItem(id);
      if (navItem) {
        navItem.children = navItem.children || [];
        navItem.children.push(item);
      }
    } else {
      navigation.push(item);
    }
  }

  removeNavigationItem(id: string) {
    const item = this.getNavigationItem(id);
    if (item) {
      let parent = this.getNavigationItemParent(id);
      parent = parent.children || parent;
      parent.slice(parent.indexOf(item), 1);
    }
  }

}
