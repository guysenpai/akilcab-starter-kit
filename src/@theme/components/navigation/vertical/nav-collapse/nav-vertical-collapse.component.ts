import { Component, OnInit, Input, HostBinding, OnDestroy } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

import { themeAnimations } from '@theme/animations';
import { NavigationItem } from '@theme/components/navigation/navigation';
import { NavigationService } from '@theme/components/navigation/navigation.service';


@Component({
  selector: 'app-nav-vertical-collapse',
  templateUrl: './nav-vertical-collapse.component.html',
  styleUrls: ['./nav-vertical-collapse.component.scss'],
  animations: [...themeAnimations]
})
export class NavVerticalCollapseComponent implements OnInit, OnDestroy {

  @Input() item: NavigationItem;

  @HostBinding('class') classes = 'nav-collapse nav-item';
  @HostBinding('class.open') isOpen = false;

  private _unsubscribeAll = new Subject();

  constructor(private _router: Router,
              private _navigationService: NavigationService) {
  }

  ngOnInit() {
    this._router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd),
      takeUntil(this._unsubscribeAll)
    ).subscribe((event: NavigationEnd) => {
      // Check if the url can be found in one of the children of this item
      if (this.isUrlInChildren(this.item, event.urlAfterRedirects)) {
        this.expand();
      } else {
        this.collapse();
      }
    });

    // Listen for collapsing of any navigation item
    this._navigationService.onItemCollapsed.pipe(takeUntil(this._unsubscribeAll)).subscribe((item: NavigationItem) => {
      if (item && item.children) {
        // Check if the clicked iten is one of the children of this item
        if (this.isChildrenOf(this.item, item)) {
          return;
        }
        // Check if the url can be found in one of the children of this item
        if (this.isUrlInChildren(this.item, this._router.url)) {
          return;
        }
        // If the clicked item is not this item, collapse...
        if (this.item !== item) {
          this.collapse();
        }
      }
    });

    if (this.isUrlInChildren(this.item, this._router.url)) {
      this.expand();
     } else {
      this.collapse();
     }
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  /**
   * Toggle collapse
   * @param event
   */
  toggleOpen(event: MouseEvent) {
    event.preventDefault();
    this.isOpen = !this.isOpen;
    // Navigation collapse toggled
    this._navigationService.onItemCollapsed.next(this.item);
    this._navigationService.onItemCollapseToggled.next();
  }

  /**
   * Expand the collapsable navigation
   */
  expand() {
    if (!this.isOpen) {
      this.isOpen = true;
      this._navigationService.onItemCollapseToggled.next();
    }
  }

  /**
   * Collapse the collapsable navigation
   */
  collapse() {
    if (this.isOpen) {
      this.isOpen = false;
      this._navigationService.onItemCollapseToggled.next();
    }
  }

  /**
   * Check if the given parent has the given item in one of its children.
   * @param parent
   * @param item
   * @returns
   */
  isChildrenOf(parent: NavigationItem, item: NavigationItem): boolean {
    if (!parent.children) {
      return false;
    }
    if (parent.children.indexOf(item) !== -1) {
      return true;
    }
    for (let i = 0; i < parent.children.length; i++) {
      const navItem = parent.children[i];
      if (navItem.children) {
        return this.isChildrenOf(navItem, item);
      }
    }
    return false;
  }

  /**
   * Check if the url can be found in one of the given parent's children.
   * @param parent
   * @param url
   * @returns
   */
  isUrlInChildren(parent: NavigationItem, url: string): boolean {
    if (!parent.children) {
      return false;
    }
    for (let i = 0; i < parent.children.length; i++) {
      const navItem = parent.children[i];
      if (navItem.children && this.isUrlInChildren(navItem, url)) {
        return true;
      }
      if (navItem.url === url || url.includes(navItem.url)) {
        return true;
      }
    }
    return false;
  }

}
