import { Component, OnInit, HostBinding, Input } from '@angular/core';

import { NavigationItem } from '@theme/components/navigation/navigation';

@Component({
  selector: 'app-nav-vertical-group',
  templateUrl: './nav-vertical-group.component.html',
  styleUrls: ['./nav-vertical-group.component.scss']
})
export class NavVerticalGroupComponent implements OnInit {

  @HostBinding('class') classes = 'nav-group nav-item';

  @Input() item: NavigationItem;

  constructor() {
  }

  ngOnInit() {
  }

}
