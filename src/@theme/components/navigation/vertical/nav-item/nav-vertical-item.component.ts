import { Component, OnInit, HostBinding, Input } from '@angular/core';

import { NavigationItem } from '@theme/components/navigation/navigation';

@Component({
  selector: 'app-nav-vertical-item',
  templateUrl: './nav-vertical-item.component.html',
  styleUrls: ['./nav-vertical-item.component.scss']
})
export class NavVerticalItemComponent implements OnInit {

  @HostBinding('class') classes = 'nav-item';

  @Input() item: NavigationItem;

  constructor() { }

  ngOnInit() {
  }

}
