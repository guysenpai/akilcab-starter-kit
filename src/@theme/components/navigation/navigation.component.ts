import { Component, OnInit, ViewEncapsulation, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { NavigationService } from '@theme/components/navigation/navigation.service';
import { Navigation } from '@theme/components/navigation/navigation';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavigationComponent implements OnInit, OnDestroy {

  @Input() layout = 'vertical';
  @Input() navigation: Navigation;

  private _unsubscribeAll = new Subject();

  constructor(private _navigationService: NavigationService) { }

  ngOnInit() {
    this.navigation = this.navigation || this._navigationService.getCurrentNavigation();
    this._navigationService.onNavigationChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
      this.navigation = this._navigationService.getCurrentNavigation();
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
