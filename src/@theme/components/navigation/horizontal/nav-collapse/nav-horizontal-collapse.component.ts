import { Component, OnInit, OnDestroy, HostBinding, Input, HostListener } from '@angular/core';
import { Subject } from 'rxjs';

import { NavigationItem } from '@theme/components/navigation/navigation';
import { Settings, ConfigService } from '@theme/services/config';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-nav-horizontal-collapse',
  templateUrl: './nav-horizontal-collapse.component.html',
  styleUrls: ['./nav-horizontal-collapse.component.scss']
})
export class NavHorizontalCollapseComponent implements OnInit, OnDestroy {

  @HostBinding('class') classes = 'nav-collapse nav-item';

  @Input() item: NavigationItem;

  isOpen = false;
  settings: Settings;

  private _unsubscribeAll = new Subject();

  constructor(private _configService: ConfigService) { }

  ngOnInit() {
    this._configService.settings.pipe(takeUntil(this._unsubscribeAll)).subscribe((newSettings: Settings) => {
      this.settings = newSettings;
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  @HostListener('mouseenter')
  open() {
    this.isOpen = true;
  }

  @HostListener('mouseleave')
  close() {
    this.isOpen = false;
  }

}
