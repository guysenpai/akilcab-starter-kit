import { Component, OnInit, Input, HostBinding } from '@angular/core';

import { NavigationItem } from '@theme/components/navigation/navigation';

@Component({
  selector: 'app-nav-horizontal-item',
  templateUrl: './nav-horizontal-item.component.html',
  styleUrls: ['./nav-horizontal-item.component.scss']
})
export class NavHorizontalItemComponent implements OnInit {

  @HostBinding('class') classes = 'nav-item';

  @Input() item: NavigationItem;

  constructor() { }

  ngOnInit() {
  }

}
