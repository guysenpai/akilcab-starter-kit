import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-loading-dialog',
  templateUrl: './loading-dialog.component.html',
  styleUrls: ['./loading-dialog.component.scss']
})
export class LoadingDialogComponent implements OnInit {

  enableActions = false;

  constructor(public dialogRef: MatDialogRef<LoadingDialogComponent>) { }

  ngOnInit() {
  }

}
