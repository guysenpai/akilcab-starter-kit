import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule, MatProgressSpinnerModule } from '@angular/material';

import { LoadingDialogComponent } from '@theme/components/loading-dialog/loading-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  exports: [LoadingDialogComponent],
  declarations: [LoadingDialogComponent],
  entryComponents: [LoadingDialogComponent]
})
export class LoadingDialogModule { }
