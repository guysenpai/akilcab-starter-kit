import { extract } from '@core/services/i18n/i18n.service';

export type Shortcuts = Shortcut[];

export interface Shortcut {
  title: string;
  translate?: string;
  type?: 'nav-item';
  icon?: string|boolean;
  badge?: ShortcutBadge;
  url?: string;
  weight?: number;
}

export interface ShortcutBadge {
  title: string;
  color?: string;
}

export const defaultShortcutsItems: Shortcuts = [
  {
    title: extract('Calendar'),
    type: 'nav-item',
    icon: 'today',
    url: '/apps/calendar'
  }
];
