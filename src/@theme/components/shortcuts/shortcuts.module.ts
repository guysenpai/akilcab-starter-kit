import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatIconModule,
  MatMenuModule,
  MatTooltipModule,
  MatFormFieldModule,
  MatListModule,
  MatDividerModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { ShortcutsComponent } from '@theme/components/shortcuts/shortcuts.component';

@NgModule({
  imports: [
    CommonModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatTooltipModule,
    RouterModule,
    TranslateModule
  ],
  exports: [ShortcutsComponent],
  declarations: [ShortcutsComponent]
})
export class ShortcutsModule { }
