import { Component, OnInit, Renderer2, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';

import { Navigation, NavigationItem } from '@theme/components/navigation/navigation';
import { NavigationService } from '@theme/components/navigation/navigation.service';
import { MatchMediaService } from '@theme/services/match-media/match-media.service';

import { Shortcuts, Shortcut, defaultShortcutsItems } from '@theme/components/shortcuts/shortcut';
import { takeUntil } from 'rxjs/operators';

const shortcutsKey = '.shortcuts';

@Component({
  selector: 'app-shortcuts',
  templateUrl: './shortcuts.component.html',
  styleUrls: ['./shortcuts.component.scss']
})
export class ShortcutsComponent implements OnInit, OnDestroy {

  @ViewChild('searchInput') searchInputField: ElementRef;
  @ViewChild('shortcuts') shortcutsEl: ElementRef;

  searching = false;
  mobileShortcutsPanelActive = false;
  // settings: Settings;
  navigation: Navigation;
  shortcutsItems: Shortcuts = [];
  navigationItems: Navigation = [];
  filteredNavigationItems: Navigation = [];
  defaultShortcutsItems: Shortcuts;

  private _unsubscribeAll = new Subject();

  constructor(private _renderer: Renderer2,
              private _cookieService: CookieService,
              private _observableMedia: ObservableMedia,
              private _matchMediaService: MatchMediaService,
              private _navigationService: NavigationService) {
    this.navigation = this._navigationService.getCurrentNavigation();
    this.defaultShortcutsItems = defaultShortcutsItems;
  }

  ngOnInit() {
    this.filteredNavigationItems = this.navigationItems = this._navigationService.getFlatNavigation(this.navigation);
    const cookieExists = this._cookieService.check(shortcutsKey);
    this.shortcutsItems = cookieExists ? JSON.parse(this._cookieService.get(shortcutsKey)) : this.defaultShortcutsItems;

    this._matchMediaService.onMediaChange.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
      if (this._observableMedia.isActive('gt-sm')) {
        this.hideMobileShortcutsPanel();
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  search(event: any) {
    const filteredValue = event.target.value.toLowerCase();
    if (filteredValue === '') {
      this.searching = false;
      this.filteredNavigationItems = this.navigationItems;
      return;
    }

    this.searching = true;
    this.filteredNavigationItems = this.navigationItems.filter((item: NavigationItem) => {
      return item.title.toLowerCase().includes(filteredValue);
    });
  }

  toggleShortcuts(event: any, itemToToggle: Shortcut) {
    event.stopPropagation();
    for (let i = 0; i < this.shortcutsItems.length; i++) {
      if (this.shortcutsItems[i].url === itemToToggle.url) {
        this.shortcutsItems.splice(i, 1);
        // Save to the cookies
        this._cookieService.set(shortcutsKey, JSON.stringify(this.shortcutsItems));
        return;
      }
    }
    this.shortcutsItems.push(itemToToggle);
    // Save to the cookies
    this._cookieService.set(shortcutsKey, JSON.stringify(this.shortcutsItems));
  }

  isInShortcuts(navigationItem: Shortcut): Shortcut|undefined {
    return this.shortcutsItems.find((item: Shortcut) => item.url === navigationItem.url);
  }

  onMenuOpen() {
    setTimeout(() => this.searchInputField.nativeElement.focus());
  }

  showMobileShortcutsPanel() {
    this.mobileShortcutsPanelActive = true;
    this._renderer.addClass(this.shortcutsEl.nativeElement, 'show-mobile-panel');
  }

  hideMobileShortcutsPanel() {
    this.mobileShortcutsPanelActive = false;
    this._renderer.removeClass(this.shortcutsEl.nativeElement, 'show-mobile-panel');
  }

}
