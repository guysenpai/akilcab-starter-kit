import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-fullscreen-toggle',
  templateUrl: './fullscreen-toggle.component.html',
  styleUrls: ['./fullscreen-toggle.component.scss']
})
export class FullscreenToggleComponent implements OnInit {

  isInFullscreen: boolean;

  constructor(@Inject(DOCUMENT) private document: any) { }

  ngOnInit() {
  }

  launchIntoFullscreen(el: any) {
    // Supports most browsers and their versions.
    const requestFullscreen = el.requestFullscreen || el.webkitRequestFullscreen || el.mozRequestFullScreen || el.msRequestFullscreen;

    if (requestFullscreen) { // Native full screen
      requestFullscreen.call(el);
    } else if (typeof window['ActiveXObject'] !== 'undefined') { // Older IE
      const wscript = new ActiveXObject('WScript.Shell');
      if (wscript !== null) {
        wscript.SendKeys('{F11}');
      }
    }
  }

  exitFullscreen(el: any) {
    // Supports most browsers and their versions.
    const cancelFullscreen = el.cancelFullScreen || el.webkitExitFullscreen || el.mozCancelFullScreen || el.exitFullscreen;

    if (cancelFullscreen) { // Native full screen
      cancelFullscreen.call(el);
    } else if (typeof window['ActiveXObject'] !== 'undefined') { // Older IE
      const wscript = new ActiveXObject('WScript.Shell');
      if (wscript !== null) {
        wscript.SendKeys('{F11}');
      }
    }
  }

  toggleFullscreen() {
    const elem = this.document.documentElement;
    this.isInFullscreen = (this.document.fullScreenElement && this.document.fullScreenElement !== null) ||
                          (this.document.mozFullScreenElement && this.document.mozFullScreenElement !== null) ||
                          (this.document.webkitFullscreenElement && this.document.webkitFullscreenElement !== null) ||
                          (this.document.mozFullScreen || this.document.webkitIsFullScreen);

    if (this.isInFullscreen) {
      this.exitFullscreen(this.document);
      this.isInFullscreen = false;
    } else {
      this.launchIntoFullscreen(elem);
      this.isInFullscreen = true;
    }
  }

}
