import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';

import { FullscreenToggleComponent } from '@theme/components/fullscreen-toggle/fullscreen-toggle.component';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    TranslateModule
  ],
  exports: [FullscreenToggleComponent],
  declarations: [FullscreenToggleComponent]
})
export class FullscreenToggleModule { }
