import { takeUntil } from 'rxjs/operators';
import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

import { ConfigService, Settings } from '@theme/services/config';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit, OnDestroy {

  collapsed = true;
  settings: Settings;
  showLoadingSpinner: boolean;

  private _unsubscribeAll = new Subject();

  @Output() input = new EventEmitter();

  constructor(private _configService: ConfigService) {
    this._configService.settings.pipe(takeUntil(this._unsubscribeAll)).subscribe((settings: Settings) => {
      this.settings = settings;
    });
  }

  ngOnInit() {
    this._configService.settings.pipe(takeUntil(this._unsubscribeAll)).subscribe((settings: Settings) => {
      this.settings = settings;
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  collapse() {
    this.collapsed = true;
  }

  expand() {
    this.collapsed = false;
  }

  search(event: any) {
    this.showLoadingSpinner = true;
    this.input.emit(event.target.value);
    setTimeout(() => this.showLoadingSpinner = false, 500);
  }

}
