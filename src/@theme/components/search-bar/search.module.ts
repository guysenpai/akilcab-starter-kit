import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatProgressSpinnerModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

import { SearchBarComponent } from '@theme/components/search-bar/search-bar.component';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    TranslateModule
  ],
  exports: [SearchBarComponent],
  declarations: [SearchBarComponent]
})
export class SearchBarModule { }
