import { Injectable } from '@angular/core';

import { SidebarComponent } from '@theme/components/sidebar/sidebar.component';
import { Logger } from '@core/services/logger/logger.service';

const log = new Logger('SidebarService');

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private _registry: {[key: string]: SidebarComponent} = {};

  constructor() { }

  register(key: string, value: SidebarComponent) {
    if (this._registry[key]) {
      log.error(`The sidebar with key "${key}" already exists. Either unregister it first or use a unique key.`);
    }
    this._registry[key] = value;
  }

  unregister(key: string) {
    if (!this._registry[key]) {
      log.warn(`The sidebar with key "${key}" does'nt exists in the registry.`);
    }
    delete this._registry[key];
  }

  getSidebar(key: string): SidebarComponent {
    if (this._registry[key]) {
      return this._registry[key];
    }
    log.warn(`The sidebar with key "${key}" does'nt exists in the registry.`);
  }

}
