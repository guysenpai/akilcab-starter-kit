import { takeUntil } from 'rxjs/operators';
import { AnimationBuilder, AnimationPlayer, animate, style } from '@angular/animations';
import {
  Component,
  OnInit,
  Input,
  HostBinding,
  ViewEncapsulation,
  HostListener,
  OnDestroy,
  Renderer2,
  ElementRef,
  ChangeDetectorRef,
  RendererStyleFlags2,
  ChangeDetectionStrategy,
  EventEmitter
} from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { Subject } from 'rxjs';

import { ConfigService, Settings } from '@theme/services/config';
import { MatchMediaService } from '@theme/services/match-media/match-media.service';

import { SidebarService } from '@theme/components/sidebar/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit, OnDestroy {

  @Input() name: string;
  @Input() position = 'left';
  @Input() lockedOpen: string;

  @HostBinding('class.open') opened: boolean;
  @HostBinding('class.locked-open') isLockedOpen: boolean;
  @HostBinding('class.unfolded') unfolded: boolean;
  @HostBinding('class.folded')
  @Input() set folded(value: boolean) {
    if (this.opened) {
      let element: HTMLElement;
      let styleProperty: string;
      this._folded = value;

      if (this.position === 'left') {
        element = this._elementRef.nativeElement.nextElementSibling;
        styleProperty = 'margin-left';
      } else if (this.position === 'right') {
        element = this._elementRef.nativeElement.previousElementSibling;
        styleProperty = 'margin-right';
      }

      if (element) {
        if (value) {
          this._renderer.setStyle(element, styleProperty, '64px', RendererStyleFlags2.Important + RendererStyleFlags2.DashCase);
        } else {
          this._renderer.removeStyle(element, styleProperty);
        }
      }
    }
  }

  get folded(): boolean {
    return this._folded;
  }

  @HostBinding('class.animations-enabled')
  get animationsEnabled() {
    return this._animationsEnabled;
  }

  invisibleOverlay = false;

  private _animationsEnabled = false;
  private _folded = false;
  private _wasActive: boolean;
  private _settings: Settings;
  private _player: AnimationPlayer;
  private _backdrop: HTMLElement | null;
  private _unsubscribeAll = new Subject();

  constructor(private _renderer: Renderer2,
              private _elementRef: ElementRef,
              private _changeDetectorRef: ChangeDetectorRef,
              private _animationBuilder: AnimationBuilder,
              private _observableMedia: ObservableMedia,
              private _matchMediaService: MatchMediaService,
              private _configService: ConfigService,
              private _sidebarService: SidebarService) {
    this.folded = false;
    this.opened = false;
  }

  ngOnInit() {
    this._configService.settings
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((settings: Settings) => this._settings = settings);

    this._sidebarService.register(this.name, this);
    this._setupVisibility();
    this._setupPosition();
    this._setupLockedOpen();
    this._setupFolded();
  }

  ngOnDestroy() {
    if (this.folded) {
      this.unfold();
    }
    this._sidebarService.unregister(this.name);
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  open() {
    if (this.opened) { return; }
    if (this.isLockedOpen) { return; }

    this._enableAnimations();
    this._showSidebar();
    this._showBackdrop();
    this.opened = true;
    this._changeDetectorRef.markForCheck();
  }

  close() {
    if (this.opened && !this.isLockedOpen) {
      this._enableAnimations();
      this._hideBackdrop();
      this.opened = false;
      this._hideSidebar();
      this._changeDetectorRef.markForCheck();
    }
  }

  toggleOpen() {
    this.opened ? this.close() : this.open();
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    if (this.folded) {
      this._enableAnimations();
      this.unfolded = true;
      this._changeDetectorRef.markForCheck();
    }
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    if (this.folded) {
      this._enableAnimations();
      this.unfolded = false;
      this._changeDetectorRef.markForCheck();
    }
  }

  fold() {
    if (!this.folded) {
      this._enableAnimations();
      this.folded = true;
      this._changeDetectorRef.markForCheck();
    }
  }

  unfold() {
    if (this.folded) {
      this._enableAnimations();
      this.folded = false;
      this._changeDetectorRef.markForCheck();
    }
  }

  toggleFold() {
    this.folded ? this.unfold() : this.fold();
  }

  private _setupVisibility() {
    this._renderer.setStyle(this._elementRef.nativeElement, 'box-shadow', 'none');
    this._renderer.setStyle(this._elementRef.nativeElement, 'visibility', 'hidden');
  }

  private _setupPosition() {
    this._renderer.addClass(this._elementRef.nativeElement, 'right' === this.position ? 'right-positioned' : 'left-positioned');
  }

  private _setupLockedOpen() {
    if (this.lockedOpen) {
      this._wasActive = false;
      this._showSidebar();
      this._matchMediaService.onMediaChange.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
        const isActive = this._observableMedia.isActive(this.lockedOpen);
        if (this._wasActive !== isActive) {
          if (isActive) {
            this.isLockedOpen = true;
            this._showSidebar();
            this.opened = true;

            if (this._settings.layout.navbar.folded) {
              this.fold();
            }
            this._hideBackdrop();
          } else {
            this.isLockedOpen = false;
            this.unfold();
            this.opened = false;
            this._hideSidebar();
          }
          this._wasActive = isActive;
        }
      });
    }
  }

  private _setupFolded() {
    let element: HTMLElement;
    let styleProperty: string;

    if (this.folded && this.opened) {
      if (this.position === 'left') {
        element = this._elementRef.nativeElement.nextElementSibling;
        styleProperty = 'margin-left';
      } else {
        element = this._elementRef.nativeElement.previousElementSibling;
        styleProperty = 'margin-right';
      }

      if (element) {
        this.fold();
        this._renderer.setStyle(element, styleProperty, '64px', RendererStyleFlags2.Important + RendererStyleFlags2.DashCase);
        this._renderer.addClass(this._elementRef.nativeElement, 'folded');
      }
    }
  }

  private _showBackdrop() {
    // Create and add backdrop
    this._backdrop = this._renderer.createElement('div');
    this._backdrop.classList.add('app-sidebar-overlay');
    if (this.invisibleOverlay) {
      this._backdrop.classList.add('app-sidebar-overlay-invisible');
    }
    this._renderer.appendChild(this._elementRef.nativeElement.parentElement, this._backdrop);

    // Create and play backdrop animation
    this._player = this._animationBuilder.build([animate('300ms ease', style({
      opacity: 1
    }))]).create(this._backdrop);
    this._player.play();

    // Listen backdrop click event
    this._backdrop.addEventListener('click', () => this.close());
    this._changeDetectorRef.markForCheck();
  }

  private _hideBackdrop() {
    if (this._backdrop) {
      this._player = this._animationBuilder.build([animate('300ms ease', style({
        opacity: 0
      }))]).create(this._backdrop);
      this._player.play();
      this._player.onDone(() => {
        if (this._backdrop) {
          this._backdrop.parentNode.removeChild(this._backdrop);
          this._backdrop = null;
        }
      });
      this._changeDetectorRef.markForCheck();
    }
  }

  private _showSidebar() {
    this._renderer.removeStyle(this._elementRef.nativeElement, 'box-shadow');
    this._renderer.removeStyle(this._elementRef.nativeElement, 'visibility');
    this._changeDetectorRef.markForCheck();
  }

  private _hideSidebar(delay: boolean = true) {
    setTimeout(() => {
      this._renderer.setStyle(this._elementRef.nativeElement, 'box-shadow', 'none');
      this._renderer.setStyle(this._elementRef.nativeElement, 'visibility', 'hidden');
    }, delay ? 300 : 0);
    this._changeDetectorRef.markForCheck();
  }

  private _enableAnimations() {
    if (!this._animationsEnabled) {
      this._animationsEnabled = true;
      this._changeDetectorRef.markForCheck();
    }
  }

}
