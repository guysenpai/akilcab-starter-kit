import { Params } from '@angular/router';

export type Breadcrumbs = Breadcrumb[];

export interface Breadcrumb {
  label: string;
  url?: string;
  params?: Params;
}
