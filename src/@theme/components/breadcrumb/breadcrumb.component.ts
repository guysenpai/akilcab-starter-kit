import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, UrlSegment, ActivatedRouteSnapshot } from '@angular/router';
import { template, templateSettings } from 'lodash';
import { Observable } from 'rxjs';
import { filter, distinctUntilChanged, map } from 'rxjs/operators';

import { Breadcrumbs, Breadcrumb } from '@theme/components/breadcrumb/breadcrumb';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  breadcrumb$: Observable<Breadcrumbs>;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    this.breadcrumb$ = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      distinctUntilChanged(),
      map(event => this.getBreadCrumbs(this.activatedRoute.root))
    );
  }

  ngOnInit() {
  }


  private getBreadCrumbs(route: ActivatedRoute, breadcrumbs?: Breadcrumbs): Breadcrumbs {
    breadcrumbs = breadcrumbs || [];

    const data = route.routeConfig && route.routeConfig.data;

    if (data && data.breadcrumb) {
      const path = this.getFullPath(route.snapshot);

      let label = typeof data.breadcrumb === 'string' ? data.breadcrumb : data.breadcrumbLabel || path;
      label = this.stringFormat(label, route.snapshot.data);

      const breadcrumb: Breadcrumb = {
        label: label,
        url: path,
        params: route.snapshot.params
      };
      breadcrumbs = [...breadcrumbs, breadcrumb];
    } else {
      breadcrumbs = [];
    }

    if (route.firstChild) {
      return this.getBreadCrumbs(route.firstChild, breadcrumbs);
    }

    return breadcrumbs;
  }

  private getFullPath(route: ActivatedRouteSnapshot): string {
    const relativePath = (segments: UrlSegment[]) => segments.reduce((a, v) => a += '/' + v.path, '');
    const fullPath = (routes: ActivatedRouteSnapshot[]) => routes.reduce((a, v) => a += '/' + relativePath(v.url), '');

    return fullPath(route.pathFromRoot);
  }

  private stringFormat(stringTemplate: string, binding: any): string {
    templateSettings.interpolate = /{{([\s\S]+?)}}/g;
    const compiled = template(stringTemplate);
    return compiled(binding);
  }

}
