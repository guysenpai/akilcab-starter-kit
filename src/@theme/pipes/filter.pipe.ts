import { Pipe, PipeTransform } from '@angular/core';

import { ThemeUtils } from '@theme/utils';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(array: any[], searchText: string, property?: string): any {
    return ThemeUtils.filterArrayByString(array, searchText);
  }

}
