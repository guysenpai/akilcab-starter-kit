import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CamelCaseToDashPipe } from '@theme/pipes/camel-case-to-dash.pipe';
import { FilterPipe } from '@theme/pipes/filter.pipe';
import { GetByIdPipe } from '@theme/pipes/get-by-id.pipe';
import { HtmlToPlainTextPipe } from '@theme/pipes/html-to-plain-text.pipe';
import { KeysPipe } from '@theme/pipes/keys.pipe';

const ThemePipes = [
  CamelCaseToDashPipe,
  FilterPipe,
  GetByIdPipe,
  HtmlToPlainTextPipe,
  KeysPipe
];

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [...ThemePipes],
  declarations: [...ThemePipes]
})
export class PipesModule { }
