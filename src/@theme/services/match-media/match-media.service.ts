import { Injectable } from '@angular/core';
import { ObservableMedia, MediaChange } from '@angular/flex-layout';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MatchMediaService {

  onMediaChange: BehaviorSubject<any>;
  activeMediaQuery = '';

  constructor(private observableMedia: ObservableMedia) {
    this.onMediaChange = new BehaviorSubject('');
    this._init();
  }


  private _init() {
    this.observableMedia.subscribe((change: MediaChange) => {
      if (this.activeMediaQuery !== change.mqAlias) {
        this.activeMediaQuery = change.mqAlias;
        this.onMediaChange.next(change.mqAlias);
      }
    });
  }

}
