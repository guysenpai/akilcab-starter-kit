import { InjectionToken } from '@angular/core';

export const CUSTOM_SETTINGS = new InjectionToken<Settings>('CUSTOM_SETTINGS');

export interface Settings {
  layout?: {
    style?: string;
    width?: 'fullwidth'|'boxed';
    content?: {
      innerScroll?: boolean;
    };
    navbar?: {
      hidden?: boolean;
      position?: 'left'|'right'|'top';
      folded?: boolean;
      background?: string;
      elevation?: string;
    };
    toolbar?: {
      hidden?: boolean;
      position?: 'above'|'below-static'|'below-fixed';
      background?: string;
      elevation?: string;
    };
    footer?: {
      hidden?: boolean;
      position?: 'above'|'below-static'|'below-fixed';
      background?: string;
      elevation?: string;
    };
  };
  customScrollbars?: boolean;
  routerAnimation?: string;
}
