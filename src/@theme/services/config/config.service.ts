import { Injectable, Inject } from '@angular/core';
import { Platform } from '@angular/cdk/platform';
import { Router, RouterEvent, NavigationStart } from '@angular/router';
import { merge, cloneDeep, isEqual } from 'lodash';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { Settings, CUSTOM_SETTINGS } from '@theme/services/config';

import { environment } from '@env/environment';

export const settingsKey = environment.settingsKey;

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  get settings(): any {
    return this._settingsSubject.asObservable();
  }

  set settings(value: any) {
    let settings = this._settingsSubject.getValue();
    settings = merge({}, settings, value);
    this._settingsSubject.next(settings);
  }

  get defaultSettings(): any {
    return this._defaultSettings;
  }

  private _defaultSettings: Settings;
  private _settingsSubject: BehaviorSubject<Settings>;

  constructor(private _router: Router,
              private _platform: Platform,
              private _cookieService: CookieService,
              @Inject(CUSTOM_SETTINGS) private _settings: Settings) {
    this._defaultSettings = this._settings;
    this.init();
  }

  getSettings(): Observable<Settings> {
    return this._settingsSubject.asObservable();
  }

  /**
   * Sets settings
   * @param settings
   */
  setSettings(value: Settings, options: {emitEvent?: boolean} = {emitEvent: true}) {
    let settings = this._settingsSubject.getValue();
    settings = merge({}, settings, value);

    if (options.emitEvent) {
      this._settingsSubject.next(settings);
    }
  }

  resetToDefaults() {
    this._settingsSubject.next(cloneDeep(this._defaultSettings));
  }


  private init() {
    // Disabled custom scrollbars if browser is mobile
    if (this._platform.ANDROID || this._platform.IOS) {
      this._defaultSettings.customScrollbars = false;
    }

    // Set the settings from the default settings
    this._settingsSubject = new BehaviorSubject(cloneDeep(this._defaultSettings));

    // Reload the default settings on every navigation start
    this._router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationStart)
    ).subscribe((event: RouterEvent) => {
      if (!isEqual(this._settingsSubject.getValue(), this._defaultSettings)) {
        const settings = cloneDeep(this._defaultSettings);
        this._settingsSubject.next(settings);
      }
    });
  }

}
