import { AnimationBuilder, style, animate, AnimationPlayer } from '@angular/animations';
import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SplashScreenService {

  splashScreenEl: HTMLElement;
  player: AnimationPlayer;

  constructor(private router: Router,
              @Inject(DOCUMENT) private document: any,
              private animationBuilder: AnimationBuilder) {
    this.splashScreenEl = this.document.body.querySelector('#app-splash-screen');

    const subscription = this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        setTimeout(() => {
          this.hide();
          subscription.unsubscribe();
        }, 0);
      }
    });
  }

  /**
   * Shows splash screen with fade in animation.
   */
  show() {
    this.player = this.animationBuilder
      .build([
        style({ opacity: '0', zIndex: '99999' }),
        animate('400ms ease', style({ opacity: '1' })),
      ])
      .create(this.splashScreenEl);

    setTimeout(() => this.player.play(), 0);
  }

  /**
   * Hides splash screen with fade out animation.
   */
  hide() {
    this.player = this.animationBuilder
      .build([
        style({ opacity: '1' }),
        animate('400ms ease', style({ opacity: '0', zIndex: '-10' })),
      ])
      .create(this.splashScreenEl);

    setTimeout(() => this.player.play(), 0);
  }

}
