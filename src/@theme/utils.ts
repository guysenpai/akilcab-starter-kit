export class ThemeUtils {

  static filterArrayByString(array: any[], searchStr: string) {
    if (searchStr === '') {
      return array;
    }

    searchStr = searchStr.toLowerCase();

    return array.filter(item => this.searchInObj(item, searchStr));
  }

  static searchInObj(itemObj: any, searchStr: string) {
    for (const prop in itemObj) {
      if (itemObj.hasOwnProperty(prop)) {
        const value = itemObj[prop];

        if (typeof value === 'string') {
          if (this.searchInString(value, searchStr)) {
            return true;
          }
        } else if (Array.isArray(value)) {
          if (this.searchInArray(value, searchStr)) {
            return true;
          }
        } else if (typeof value === 'object') {
          if (this.searchInObj(value, searchStr)) {
            return true;
          }
        }
      }
    }
  }

  static searchInArray(array: any[], searchStr: string) {
    for (let i = 0; i < array.length; i++) {
      const value = array[i];
      if (typeof value === 'string') {
        if (this.searchInString(value, searchStr)) {
          return true;
        }
      } else if (typeof value === 'object') {
        if (this.searchInObj(value, searchStr)) {
          return true;
        }
      }
    }
  }

  static searchInString(value: string, searchStr: string) {
    return value.toLowerCase().includes(searchStr);
  }

  /**
   * Generate a unique id
   * @static
   * @returns
   */
  static generateGUID(): string {
    const s4 = () => Math.floor((1 + Math.random()) * 65536).toString(16).substring(1);
    return s4() + s4();
  }

  /**
   * Add/Remove item from an array.
   * @static
   * @param item Item to add or remove.
   * @param array
   */
  static toggleArray(item: any, array: any[]) {
    if (array.indexOf(item) === -1) {
      array.push(item);
    } else {
      array.splice(array.indexOf(item), 1);
    }
  }

  /**
   * Tranforms text in dashed case.
   * @static
   * @param text text to handleize
   */
  static handleize(text: string): string {
    return text.toString().toLowerCase()
               .replace(/\s+/g, '-')          // Replace spaces with '-'
               .replace(/[^\w\-]+/g, '')      // Remove all non-word chars
               .replace(/\-\-+/g, '-')        // Replace multiple '-' with single '-'
               .replace(/^-+/g, '')           // Trim '-' from start of text
               .replace(/-+$/g, '');          // Trim '-' from end of text
  }
}
