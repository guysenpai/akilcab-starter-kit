import { NgModule, ModuleWithProviders } from '@angular/core';

import { BreadcrumbModule } from '@theme/components/breadcrumb/breadcrumb.module';
import { ConfirmDialogModule } from '@theme/components/confirm-dialog/confirm-dialog.module';
import { FullscreenToggleModule } from '@theme/components/fullscreen-toggle/fullscreen-toggle.module';
import { LoadingDialogModule } from '@theme/components/loading-dialog/loading-dialog.module';
import { NavigationModule } from '@theme/components/navigation/navigation.module';
import { NotificationDialogModule } from '@theme/components/notification-dialog/notification-dialog.module';
import { SearchBarModule } from '@theme/components/search-bar/search.module';
import { ShortcutsModule } from '@theme/components/shortcuts/shortcuts.module';
import { SidebarModule } from '@theme/components/sidebar/sidebar.module';
import { WidgetModule } from '@theme/components/widget/widget.module';

import { IfOnDomModule } from '@theme/directives/if-on-dom/if-on-dom.module';
import { PerfectScrollbarModule } from '@theme/directives/perfect-scrollbar/perfect-scrollbar.module';
import { MatSidenavHelperModule } from '@theme/directives/mat-sidenav-helper/mat-sidenav-helper.module';

import { PipesModule } from '@theme/pipes/pipes.module';

import { CUSTOM_SETTINGS, Settings } from '@theme/services/config';

const ThemeModules = [
  // Components modules
  BreadcrumbModule,
  ConfirmDialogModule,
  FullscreenToggleModule,
  LoadingDialogModule,
  NavigationModule,
  NotificationDialogModule,
  SearchBarModule,
  ShortcutsModule,
  SidebarModule,
  WidgetModule,
  // Directive modules
  IfOnDomModule,
  MatSidenavHelperModule,
  PerfectScrollbarModule,
  PipesModule,
];

const ThemeProviders: any[] = [];

@NgModule({
  imports: [...ThemeModules],
  exports: [...ThemeModules],
  declarations: [],
  providers: [...ThemeProviders]
})
export class ThemeModule {

  static forRoot(options: Settings): ModuleWithProviders {
    return {
      ngModule: ThemeModule,
      providers: [
        { provide: CUSTOM_SETTINGS, useValue: options }
      ]
    };
  }

}
