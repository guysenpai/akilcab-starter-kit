import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class MatSidenavHelperService {

  sidenavInstances: MatSidenav[];

  constructor() {
    this.sidenavInstances = [];
  }

  setSidenav(id: string, instance: MatSidenav) {
    this.sidenavInstances[id] = instance;
  }

  getSidenav(id: string): MatSidenav {
    return this.sidenavInstances[id];
  }

}
