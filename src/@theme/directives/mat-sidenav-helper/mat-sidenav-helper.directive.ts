import { Directive, HostBinding, OnInit, OnDestroy, Input } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { MatSidenav } from '@angular/material';
import { Subscription } from 'rxjs';

import { MatSidenavHelperService } from '@theme/directives/mat-sidenav-helper/mat-sidenav-helper.service';
import { MatchMediaService } from '@theme/services/match-media/match-media.service';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[matSidenavHelper]'
})
export class MatSidenavHelperDirective implements OnInit, OnDestroy {

  matchMediaSubscription: Subscription;

  @HostBinding('class.mat-is-locked-open') isLockedOpen = true;

  // tslint:disable-next-line:no-input-rename
  @Input('matSidenavHelper') id: string;
  // tslint:disable-next-line:no-input-rename
  @Input('matIsLockedOpen') matIsLockedOpenBreakpoint: string;

  constructor(private matSidenavService: MatSidenavHelperService,
              private matchMediaService: MatchMediaService,
              private observableMedia: ObservableMedia,
              private matSidenav: MatSidenav) { }

  ngOnInit() {
    this.matSidenavService.setSidenav(this.id, this.matSidenav);
    if (this.observableMedia.isActive(this.matIsLockedOpenBreakpoint)) {
      this.isLockedOpen = true;
      this.matSidenav.mode = 'side';
      this.matSidenav.toggle(true);
    } else {
      this.isLockedOpen = false;
      this.matSidenav.mode = 'over';
      this.matSidenav.toggle(false);
    }

    this.matchMediaSubscription = this.matchMediaService.onMediaChange.subscribe(() => {
      if (this.observableMedia.isActive(this.matIsLockedOpenBreakpoint)) {
        this.isLockedOpen = true;
        this.matSidenav.mode = 'side';
        this.matSidenav.toggle(true);
      } else {
        this.isLockedOpen = false;
        this.matSidenav.mode = 'over';
        this.matSidenav.toggle(false);
      }
    });
  }

  ngOnDestroy() {
    this.matchMediaSubscription.unsubscribe();
  }

}
