import { Directive, HostListener, Input } from '@angular/core';

import { MatSidenavHelperService } from '@theme/directives/mat-sidenav-helper/mat-sidenav-helper.service';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[matSidebarToggler]'
})
export class MatSidebarTogglerDirective {

  // tslint:disable-next-line:no-input-rename
  @Input('matSidebarToggler') id: string;

  constructor(private _matSidenavHelperService: MatSidenavHelperService) { }

  @HostListener('click')
  onClick() {
    this._matSidenavHelperService.getSidenav(this.id).toggle();
  }

}
