import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSidenavModule } from '@angular/material';

import { MatSidenavHelperDirective } from '@theme/directives/mat-sidenav-helper/mat-sidenav-helper.directive';
import { MatSidebarTogglerDirective } from '@theme/directives/mat-sidenav-helper/mat-sidenav-toggler.directive';

@NgModule({
  imports: [
    FlexLayoutModule,
    MatSidenavModule
  ],
  exports: [MatSidenavHelperDirective, MatSidebarTogglerDirective],
  declarations: [MatSidenavHelperDirective, MatSidebarTogglerDirective]
})
export class MatSidenavHelperModule { }
