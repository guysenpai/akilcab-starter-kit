import { TestBed, inject } from '@angular/core/testing';

import { MatSidenavHelperService } from '@theme/directives/mat-sidenav-helper/mat-sidenav-helper.service';

describe('MatSidenavHelperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MatSidenavHelperService]
    });
  });

  it('should be created', inject([MatSidenavHelperService], (service: MatSidenavHelperService) => {
    expect(service).toBeTruthy();
  }));
});
