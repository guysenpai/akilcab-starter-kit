import { Directive, AfterContentChecked, TemplateRef, ViewContainerRef, ElementRef, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[IfOnDom]'
})
export class IfOnDomDirective implements AfterContentChecked {

  isCreated = false;

  constructor(private _elementRef: ElementRef,
              private _templateRef: TemplateRef<any>,
              private _viewContainerRef: ViewContainerRef,
              @Inject(DOCUMENT) private _document: any) { }

  ngAfterContentChecked() {
    if (this._document.body.contains(this._elementRef.nativeElement) && !this.isCreated) {
      setTimeout(() => this._viewContainerRef.createEmbeddedView(this._templateRef), 300);
      this.isCreated = true;
    } else if (this.isCreated && !this._document.body.contains(this._elementRef.nativeElement)) {
      this._viewContainerRef.clear();
      this.isCreated = false;
    }
  }

}
