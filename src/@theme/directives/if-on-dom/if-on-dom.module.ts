import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IfOnDomDirective } from '@theme/directives/if-on-dom/if-on-dom.directive';

@NgModule({
  imports: [CommonModule],
  exports: [IfOnDomDirective],
  declarations: [IfOnDomDirective]
})
export class IfOnDomModule { }
