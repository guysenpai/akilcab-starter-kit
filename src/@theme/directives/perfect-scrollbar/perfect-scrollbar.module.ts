import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PerfectScrollbarDirective } from '@theme/directives/perfect-scrollbar/perfect-scrollbar.directive';

@NgModule({
  imports: [
    RouterModule
  ],
  exports: [PerfectScrollbarDirective],
  declarations: [PerfectScrollbarDirective]
})
export class PerfectScrollbarModule { }
