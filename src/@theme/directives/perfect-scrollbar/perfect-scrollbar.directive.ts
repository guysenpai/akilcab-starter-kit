import { Platform } from '@angular/cdk/platform';
import { AfterViewInit, Directive, ElementRef, HostListener, OnDestroy, OnInit, Input } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { debounce, merge } from 'lodash';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

import PerfectScrollbar from 'perfect-scrollbar';

import { PerfectScrollbarOptions } from '@theme/directives/perfect-scrollbar/perfect-scrollbar-options';
import { ConfigService, Settings } from '@theme/services/config';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[perfectScrollbar]'
})
export class PerfectScrollbarDirective implements OnInit, AfterViewInit, OnDestroy {

  public isDisableCustomScrollbars = false;
  public isMobile = false;
  public isInitialized = false;
  public ps: PerfectScrollbar;

  get perfectScrollbarOptions(): PerfectScrollbarOptions {
    return this._options;
  }

  @Input() set perfectScrollbarOptions(value: PerfectScrollbarOptions) {
    this._options = merge({}, this._options, value);
  }

  get enabled() {
    return this._enabled;
  }

  @Input('perfectScrollbar') set enabled(value: any) {
    if (value === '') {
      value = true;
    }

    if (this.enabled !== value) {
      this._enabled = value;
      this.enabled ? this._init() : this._destroy();
    }
  }

  private _enabled = false;
  private _options: PerfectScrollbarOptions;
  private _debounceUpdate: () => void;
  private _unsubscribeAll: Subject<any>;

  constructor(private _router: Router,
              private _platform: Platform,
              private _elementRef: ElementRef,
              private _configService: ConfigService) {
    this._debounceUpdate = debounce(this.update, 150);
    this._options = { updateOnRouteChange: false };
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this._configService.settings.pipe(takeUntil(this._unsubscribeAll)).subscribe((settings: Settings) => {
      this.enabled = settings.customScrollbars;
    });

    if (this.perfectScrollbarOptions.updateOnRouteChange) {
      this._router.events.pipe(
        takeUntil(this._unsubscribeAll),
        filter((event: RouterEvent) => event instanceof NavigationEnd)
      ).subscribe(() => {
        setTimeout(() => {
          this.scrollToTop();
          this.update();
        }, 0);
      });
    }
  }

  ngOnDestroy() {
    this._destroy();
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  @HostListener('document:click', ['$event.target'])
  documentClick(event: MouseEvent) {
    if (this.isInitialized && this.ps) {
      this.ps.update();
    }
  }

  update() {
    if (!this.isInitialized) {
      return;
    }

    // Update the perfect-scrollbar
    this.ps.update();
  }

  destroy() {
    this.ngOnDestroy();
  }

  scrollToX(x: number, speed?: number) {
    this.animateScrolling('scrollLeft', x, speed);
  }

  scrollToY(y: number, speed?: number) {
    this.animateScrolling('scrollTop', y, speed);
  }

  scrollToTop(offset?: number, speed?: number) {
    this.animateScrolling('scrollTop', offset || 0, speed);
  }

  scrollToLeft(offset?: number, speed?: number) {
    this.animateScrolling('scrollLeft', offset || 0, speed);
  }

  scrollToRight(offset?: number, speed?: number) {
    const width = this._elementRef.nativeElement.scrollWidth;
    this.animateScrolling('scrollLeft', width - (offset || 0), speed);
  }

  scrollToBottom(offset?: number, speed?: number) {
    const height = this._elementRef.nativeElement.scrollHeight;
    this.animateScrolling('scrollTop', height - (offset || 0), speed);
  }

  animateScrolling(target: string, value: number, speed?: number) {
    if (!speed) {
      this._elementRef.nativeElement[target] = value;

      // PS has weird event sending order, this is a workaround for that
      this.update();
      this.update();
    } else if (value !== this._elementRef.nativeElement[target]) {
      let newValue = 0;
      let scrollCount = 0;

      let oldTimestamp = performance.now();
      let oldValue = this._elementRef.nativeElement[target];

      const cosParameter = (oldValue - value) / 2;

      const step = (newTimestamp: number) => {
        scrollCount += Math.PI / (speed / (newTimestamp - oldTimestamp));

        newValue = Math.round(value + cosParameter + cosParameter * Math.cos(scrollCount));

        // Only continue animation if scroll position has not changed
        if (this._elementRef.nativeElement[target] === oldValue) {
          if (scrollCount >= Math.PI) {
            this._elementRef.nativeElement[target] = value;

            // PS has weird event sending order, this is a workaround for that
            this.update();
            this.update();
          } else {
            this._elementRef.nativeElement[target] = oldValue = newValue;
            oldTimestamp = newTimestamp;

            window.requestAnimationFrame(step);
          }
        }
      };

      window.requestAnimationFrame(step);
    }
  }


  private _init() {
    if (!this.isInitialized) {
      if (this._platform.ANDROID || this._platform.IOS) {
        this.isMobile = true;
      }
    }

    if (!this.isMobile) {
      this.isInitialized = true;
      this.ps = new PerfectScrollbar(this._elementRef.nativeElement, this.perfectScrollbarOptions);
    }
  }

  private _destroy() {
    if (!this.isInitialized || !this.ps) {
      return;
    }

    // Destroy the perfect-scrollbar
    this.ps.destroy();
    this.ps = null;
    this.isInitialized = false;
  }

  private _updateOnResize() {
    this._debounceUpdate();
  }

}
