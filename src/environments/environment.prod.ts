// `.env.ts` is generated by the `npm run env` command
import env from '@env/.env';

export const environment = {
  production: true,
  hmr: false,
  version: env.npm_package_version,
  settingsKey: 'akilcab.settings',
  credentialsKey: 'akilcab.credentials',
  app: {
    name: 'AKILCAB',
    logo: 'assets/images/logos/akilcab.svg',
    key: '545a1c03cc2d2281.48121707app001MA',
  },
  serverUrl: 'https://api-prod.macomptaohada.com',
  akilCabHubUrl: 'https://akilcab.com',
  redirectBackUrl: 'https://macomptaohada.com',
  socketHost: 'http://localhost:6001',
  defaultLanguage: 'en-US',
  supportedLanguages: [
    'en-US',
    'fr-FR'
  ]
};
