/*
 * Extra typings definitions
 */

/* Allow .json files imports */
declare module '*.json';

/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

// HttpClient is declared in a re-exported module, so we have to extend the original module to make it work properly
// (see https://github.com/Microsoft/TypeScript/issues/13897)
declare module '@angular/common/http/src/client' {

  // Augment HttpClient with the added configuration methods from HttpService, to allow in-place replacement of
  // HttpClient with HttpService using dependency injection
  export interface HttpClient {

    /**
     * Enables caching for this request.
     * @param forceUpdate Forces request to be made and updates cache entry.
     * @return The new instance.
     */
    cache(forceUpdate?: boolean): HttpClient;

    /**
     * Skips default error handler for this request.
     * @return The new instance.
     */
    skipErrorHandler(): HttpClient;

    /**
     * Do not use API prefix for this request.
     * @returns The new instance.
     */
    disableApiPrefix(): HttpClient;

  }

}

// ActiveXObject definition
declare var ActiveXObject: ActiveXObject;
interface ActiveXObject {
  new (type: string): ActiveXObject;
  SendKeys(key: string): void;
}
