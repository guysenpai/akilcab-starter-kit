import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initialism'
})
export class InitialismPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const nouns = value.split(' ');

    if (nouns.length > 1) {
      return nouns[0].charAt(0).toUpperCase() + nouns[1].charAt(0).toUpperCase();
    } else {
      return nouns[0].charAt(0).toUpperCase();
    }
  }

}
