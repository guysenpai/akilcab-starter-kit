import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'table-skeleton',
  templateUrl: './table-skeleton.component.html',
  styleUrls: ['./table-skeleton.component.scss']
})
export class TableSkeletonComponent implements OnInit {

  @Input() displayedColumns: any[] = [];

  dataSource: MatTableDataSource<any>;

  constructor() { }

  ngOnInit() {
    let data: any[] = [];
    for (let i = 0; i < 10; i++) {
      data = [...data, {}];
    }
    this.dataSource = new MatTableDataSource(data);
  }
}
