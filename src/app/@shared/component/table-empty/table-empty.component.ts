import { Component, OnInit, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'table-empty',
  templateUrl: './table-empty.component.html',
  styleUrls: ['./table-empty.component.scss']
})
export class TableEmptyComponent implements OnInit {

  @Input() message = 'Aucune donnée disponible';

  constructor() { }

  ngOnInit() {
  }

}
