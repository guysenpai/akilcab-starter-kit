import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'ngx-moment';

import { MaterialModule } from '@shared/modules/material.module';

import { TableSkeletonComponent } from './component/table-skeleton/table-skeleton.component';
import { TableEmptyComponent } from './component/table-empty/table-empty.component';
import { InitialismPipe } from './pipes/initialism.pipe';

const angularModules: any[] = [
  CommonModule,
  FormsModule,
  FlexLayoutModule,
  MaterialModule,
  ReactiveFormsModule,
  RouterModule
];

const thirdPartyModules: any[] = [
  MomentModule,
  TranslateModule
];

const sharedComponents: any[] = [
  TableEmptyComponent,
  TableSkeletonComponent
];

const sharedEntryComponents: any[] = [];

const sharedDirectives: any[] = [];

const sharedPipes: any[] = [
  InitialismPipe
];


@NgModule({
  imports: [...angularModules, ...thirdPartyModules],
  exports: [...angularModules, ...thirdPartyModules, ...sharedComponents, sharedDirectives, sharedPipes],
  declarations: [...sharedComponents, ...sharedDirectives, ...sharedPipes],
  entryComponents: [...sharedEntryComponents]
})
export class SharedModule { }
