import { Settings } from '@theme/services/config';

export const appSettings: Settings = {
  layout: {
    style: 'vertical-layout',
    width: 'fullwidth',
    content: {
      innerScroll: true
    },
    navbar: {
      hidden: false,
      position: 'left',
      folded: false,
      background: 'mat-white-500-bg',
      elevation: 'mat-elevation-z0'
    },
    toolbar: {
      hidden: false,
      position: 'below-fixed',
      background: 'mat-white-500-bg',
      elevation: 'mat-elevation-z0'
    },
    footer: {
      hidden: false,
      position: 'below-static',
      background: 'mat-white-500-bg',
      elevation: 'mat-elevation-z0'
    }
  },
  customScrollbars: true,
  routerAnimation: 'fadeIn'
};
