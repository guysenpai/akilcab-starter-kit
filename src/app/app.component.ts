import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { merge, Subject } from 'rxjs';
import { filter, map, mergeMap, takeUntil } from 'rxjs/operators';

import { environment } from '@env/environment';
import { ConfigService } from '@theme/services/config';
import { SseService } from '@core/services/sse/sse.service';
import { Logger } from '@core/services/logger/logger.service';
import { I18nService } from '@core/services/i18n/i18n.service';
import { SwUpdateService } from '@core/services/sw-update/sw-update.service';
import { SvgIconService } from '@app/@core/services/svg-icon/svg-icon.service';
import { SplashScreenService } from '@theme/services/splash-screen/splash-screen.service';
import { AuthenticationService } from '@core/services/authentication/authentication.service';
import { NavigationService } from '@theme/components/navigation/navigation.service';
import { Navigation } from '@theme/components/navigation/navigation';
import { cabinetNavigationMenu, clientNavigationMenu } from '@app/navigation-menu';

const log = new Logger('AppComponent');

@Component({
  selector: 'akilcab-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  navigation: Navigation = [];

  private _unsubscribeAll = new Subject();

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _matSnackBar: MatSnackBar,
              private _titleService: Title,
              private _i18nService: I18nService,
              private _cookieService: CookieService,
              private _configService: ConfigService,
              private _svgIconService: SvgIconService,
              private _swUpdateService: SwUpdateService,
              private _translateService: TranslateService,
              private _navigationService: NavigationService,
              private _splashScreenService: SplashScreenService,
              private _authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    // Setup logger
    if (environment.production) {
      Logger.enableProductionMode();
    }

    log.debug('init');

    // Setup translations
    this._i18nService.init(environment.defaultLanguage, environment.supportedLanguages);

    const onNavigationEnd = this._router.events.pipe(filter(event => event instanceof NavigationEnd));

    // Change page title on navigation or language change, based on route data
    merge(this._translateService.onLangChange, onNavigationEnd).pipe(
      map(() => {
        let route = this._activatedRoute;
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter(route => route.outlet === 'primary'),
      mergeMap(route => route.data),
      takeUntil(this._unsubscribeAll)
    ).subscribe(event => {
      const title = event['title'];
      if (title) {
        this._titleService.setTitle(this._translateService.instant(title));
      }
    });

    // Setup user navigation
    this._authenticationService.onSessionChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(session => {
      if (session && session.profile) {
        switch (session.profile.profil_type) {
          case 'PROFILE_ACCOUNTINGFIRM':
            this.navigation = cabinetNavigationMenu;
            break;
          case 'PROFILE_CUSTOMER':
            this.navigation = clientNavigationMenu;
            break;
          default:
            this.navigation = [];
            break;
        }
      }

      if (!this._navigationService.hasNavigation('main')) {
        this._navigationService.register('main', this.navigation);
      } else {
        this._navigationService.unregister('main');
        this._navigationService.register('main', this.navigation);
      }

      if (!this._navigationService.getCurrentNavigation()) {
        log.debug('Set Current navigation.');
        this._navigationService.setCurrentNavigation('main');
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
