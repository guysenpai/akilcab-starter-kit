export type Languages = Language[];

export interface Language {
  id: string;
  title: string;
  flag: string;
}
