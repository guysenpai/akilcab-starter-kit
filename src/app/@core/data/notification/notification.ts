export type Notifications = Notification[];

export interface Notification {
  id?: number;
}
