import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { orderBy } from 'lodash';
import { Observable, BehaviorSubject, merge } from 'rxjs';
import { map } from 'rxjs/operators';

import { ThemeUtils } from '@theme/utils';
import { Example } from '@core/data/example/example';
import { ExampleService } from '@core/data/example/example.service';

export class ExampleDataSource extends DataSource<Example> {

  get filteredData(): Example[] {
    return this._filteredDataChange.value;
  }

  set filteredData(value: Example[]) {
    this._filteredDataChange.next(value);
  }

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(value: string) {
    this._filterChange.next(value);
  }

  private _filterChange = new BehaviorSubject('');
  private _filteredDataChange = new BehaviorSubject<Example[]>([]);

  constructor(private _exampleService: ExampleService,
              private _matPaginator: MatPaginator,
              private _matSort: MatSort) {
    super();
    this.filteredData = this._exampleService.exampleArray;
  }

  connect(): Observable<Example[]> {
    return merge(this._exampleService.onExampleArrayChanged, this._matPaginator.page, this._filterChange, this._matSort.sortChange)
          .pipe(map(() => {
            let data = this._exampleService.exampleArray.slice();
            data = this.filterData(data);
            this.filteredData = data.slice();
            data = this.sortData(data);
            return data.splice(this._matPaginator.pageIndex * this._matPaginator.pageSize, this._matPaginator.pageSize);
          }));
  }

  filterData(data: Example[]): Example[] {
    return this.filter ? ThemeUtils.filterArrayByString(data, this.filter) : data;
  }

  sortData(data: Example[]) {
    const active = this._matSort.active;
    const direction = this._matSort.direction;

    return active && direction !== '' ? data.sort((a, b) => {
      const valueA = a[active];
      const valueB = b[active];

      return ((isNaN(+valueA) ? valueA : +valueA) < (isNaN(+valueB) ? valueB : +valueB) ? -1 : 1) * (direction === 'asc' ? 1 : -1);
    }) : data;
  }

  disconnect() {
    this._filterChange.complete();
  }

}
