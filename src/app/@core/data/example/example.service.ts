import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { Example } from './example';

@Injectable({
  providedIn: 'root'
})
export class ExampleService {

  onExampleArrayChanged: BehaviorSubject<Example[]>;

  get exampleArray(): Example[] {
    return this.onExampleArrayChanged.value;
  }

  set exampleArray(value: Example[]) {
    this.onExampleArrayChanged.next(value);
  }

  private endpoint = '/customer-folders';

  constructor(private http: HttpClient) {
    this.onExampleArrayChanged = new BehaviorSubject([]);
  }

  getExampleData(): Observable<Example> {
    return this.http.get(`${this.endpoint}`).pipe(
      map((response: any) => response.data),
      tap((examples: Example[]) => this.exampleArray = examples),
      catchError((err: any) => throwError(err))
    );
  }

}
