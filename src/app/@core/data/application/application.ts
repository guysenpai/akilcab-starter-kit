export type Applications = Application[];

export interface Application {
  id?: number;
  code?: string;
  wording?: string;
  cle?: string;
  logo?: string;
  link?: string;
  enabled?: string;
}
