import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, forkJoin, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Applications } from './application';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  applications: Applications;
  onApplicationsChanged: BehaviorSubject<Applications>;

  private applicationsUrl = '/applications';

  constructor(private http: HttpClient) {
    this.onApplicationsChanged = new BehaviorSubject([]);
  }

  getApplications(): Observable<Applications> {
    return this.http.get(`${this.applicationsUrl}`).pipe(
      map((response: any) => response.data),
      map((applications: Applications) => {
        this.applications = applications;
        this.onApplicationsChanged.next(this.applications);
        return this.applications;
      }),
      catchError((err: any) => throwError(err))
    );
  }

}
