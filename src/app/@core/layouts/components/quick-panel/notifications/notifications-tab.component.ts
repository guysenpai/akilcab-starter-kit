import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'notifications-tab',
  templateUrl: './notifications-tab.component.html',
  styleUrls: ['./notifications-tab.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NotificationsTabComponent implements OnInit, OnDestroy {

  private _unsubscribeAll = new Subject();

  constructor(private _http: HttpClient) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
