import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'overview-tab',
  templateUrl: './overview-tab.component.html',
  styleUrls: ['./overview-tab.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OverviewTabComponent implements OnInit, OnDestroy {

  date = new Date();
  settings = {
    notify: true,
    cloud: false,
    retro: true
  };

  private _unsubscribeAll = new Subject();

  constructor(private _http: HttpClient) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  onChange(event: any) {
    console.log(event);
  }

}
