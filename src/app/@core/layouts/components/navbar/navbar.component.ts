import { Component, OnInit, ViewEncapsulation, Input, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

import { Navigation } from '@theme/components/navigation/navigation';
import { NavigationService } from '@theme/components/navigation/navigation.service';
import { PerfectScrollbarDirective } from '@theme/directives/perfect-scrollbar/perfect-scrollbar.directive';
import { SidebarService } from '@theme/components/sidebar/sidebar.service';
import { ConfigService, Settings } from '@theme/services/config';

import { environment } from '@env/environment';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit, OnDestroy {

  @Input() layout = 'vertical';

  app = {
    name: (environment.app.name).toLocaleUpperCase(),
    logo: environment.app.logo
  };
  navigationServiceWatcher: Subscription;
  perfectScrollbarUpdateTimeout: any;
  navigation: Navigation;
  settings: Settings;

  private _perfectScrollbar: PerfectScrollbarDirective;
  private _unsubscribeAll = new Subject();

  set directive(directive: any) {
    if (directive) {
      this._perfectScrollbar = directive;
      this._navigationService.onItemCollapseToggled.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
        this.perfectScrollbarUpdateTimeout = setTimeout(() => {
          this._perfectScrollbar.update();
        }, 310);
      });
    }
  }

  constructor(private _router: Router,
              private _configService: ConfigService,
              private _sidebarService: SidebarService,
              private _navigationService: NavigationService) {
  }

  ngOnInit() {
    this._router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd),
      takeUntil(this._unsubscribeAll)
    ).subscribe(() => {
      if (this._sidebarService.getSidebar('navbar')) {
        this._sidebarService.getSidebar('navbar').close();
      }
    });

    this._configService.settings.pipe(takeUntil(this._unsubscribeAll)).subscribe((settings: Settings) => {
      this.settings = settings;
    });

    this._navigationService.onNavigationChanged.pipe(filter(key => key !== null)).subscribe(() => {
      this.navigation = this._navigationService.getCurrentNavigation();
    });
  }

  ngOnDestroy() {
    if (this.perfectScrollbarUpdateTimeout) {
      clearTimeout(this.perfectScrollbarUpdateTimeout);
    }
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  toggleSidebarOpened(sidebar: string) {
    this._sidebarService.getSidebar(sidebar).toggleOpen();
  }

  toggleSidebarFolded(sidebar: string) {
    this._sidebarService.getSidebar(sidebar).toggleFold();
  }

  isSidebarFolded(sidebar: string): boolean {
    return this._sidebarService.getSidebar(sidebar).folded;
  }

}
