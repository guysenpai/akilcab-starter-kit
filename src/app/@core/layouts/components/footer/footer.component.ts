import { takeUntil } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { ConfigService, Settings } from '@theme/services/config';

import { environment } from '@env/environment';
import { Subject } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {

  version = environment.version;
  settings: Settings;

  private _unsubscribeAll = new Subject();

  constructor(private _configService: ConfigService) { }

  ngOnInit() {
    this._configService.getSettings().pipe(takeUntil(this._unsubscribeAll)).subscribe((settings) => this.settings = settings);
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
