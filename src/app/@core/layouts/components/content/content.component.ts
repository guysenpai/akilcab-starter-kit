import { Component, OnInit, HostBinding, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, RouterEvent, NavigationEnd } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';

import { themeAnimations } from '@theme/animations';
import { ConfigService, Settings } from '@theme/services/config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [...themeAnimations]
})
export class ContentComponent implements OnInit, OnDestroy {

  settings: Settings;

  @HostBinding('@routerTransitionUp') routerAnimationUp = false;
  @HostBinding('@routerTransitionDown') routerAnimationDown = false;
  @HostBinding('@routerTransitionRight') routerAnimationRight = false;
  @HostBinding('@routerTransitionLeft') routerAnimationLeft = false;
  @HostBinding('@routerTransitionFade') routerAnimationFade = false;

  private _unsubscribeAll = new Subject();

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _configService: ConfigService) {
  }

  ngOnInit() {
    this._router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd),
      takeUntil(this._unsubscribeAll)
    ).subscribe(() => {
      // switch (this.settings.routerAnimation) {
      //   case 'fadeIn':
      //     this.routerAnimationFade = !this.routerAnimationFade;
      //     break;
      //   case 'slideUp':
      //     this.routerAnimationUp = !this.routerAnimationUp;
      //     break;
      //   case 'slideDown':
      //     this.routerAnimationDown = !this.routerAnimationDown;
      //     break;
      //   case 'slideRight':
      //     this.routerAnimationRight = !this.routerAnimationRight;
      //     break;
      //   case 'slideLeft':
      //     this.routerAnimationLeft = !this.routerAnimationLeft;
      //     break;
      // }
    });

    this._configService.settings.pipe(takeUntil(this._unsubscribeAll)).subscribe((newSettings: Settings) => {
      this.settings = newSettings;
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
