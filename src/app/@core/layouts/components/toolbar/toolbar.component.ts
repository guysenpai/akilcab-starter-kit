import { Location, DOCUMENT } from '@angular/common';
import { Component, OnInit, OnDestroy, Input, Inject } from '@angular/core';
import { Router, RouterEvent, NavigationStart, NavigationEnd } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

import { SidebarService } from '@theme/components/sidebar/sidebar.service';
import { Settings, ConfigService } from '@theme/services/config';

import { Application } from '@core/data/application';
import { Notification } from '@core/data/notification';

import { AuthenticationService } from '@core/services/authentication/authentication.service';
import { Logger } from '@core/services/logger/logger.service';

import { environment } from '@env/environment';

const log = new Logger('ToolbarComponent');

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {

  @Input() user = {
    avatar_url: 'assets/images/avatars/profile.jpg',
    civility: 'M.',
    first_name: 'Guy',
    last_name: 'Senpai',
    username: 'guysenpai'
  };
  @Input() cabinet = {
    physical_person: false,
    first_name: 'Guy',
    last_name: 'Senpai',
    social_reason: 'AfroGeekLand Dev Studio'
  };
  @Input() apps: Application[] = [];
  @Input() notifications: Notification[] = [];

  app = {
    name: (environment.app.name).toLocaleUpperCase(),
    logo: environment.app.logo
  };
  hiddenNavbar: boolean;
  rightNavbar: boolean;
  horizontalNavbar: boolean;
  toolbarElevation: string;
  showLoadingBar: boolean;

  private _unsubscribeAll = new Subject();

  constructor(private _router: Router,
              private _location: Location,
              @Inject(DOCUMENT) private _document: any,
              private _configService: ConfigService,
              private _sidebarService: SidebarService,
              private _authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this._router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationStart),
      takeUntil(this._unsubscribeAll)
    ).subscribe((event: NavigationStart) => {
      this.showLoadingBar = true;
    });

    this._router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd),
      takeUntil(this._unsubscribeAll)
    ).subscribe((event: NavigationEnd) => {
      this.showLoadingBar = false;
    });

    this._configService.settings.pipe(takeUntil(this._unsubscribeAll)).subscribe((config: Settings) => {
      this.horizontalNavbar = config.layout.navbar.position === 'top';
      this.rightNavbar = config.layout.navbar.position === 'right';
      this.hiddenNavbar = config.layout.navbar.hidden === true;
      this.toolbarElevation = config.layout.toolbar.elevation;
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  toggleSidebarOpen(sidebarName: string) {
    this._sidebarService.getSidebar(sidebarName).toggleOpen();
  }

  search(value: string) {
    log.debug(`Search value: ${value}`);
  }

  switchTo(app: Application) {
    return this._location.go(app.link);
  }

  logout() {
    this._authenticationService.logout().pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
      this._document.location.href = this._authenticationService.redirectUrl;
    }, () => this._document.location.href = this._authenticationService.redirectUrl);
  }

}
