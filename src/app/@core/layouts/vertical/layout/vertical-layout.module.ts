import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';
import { ThemeModule } from '@theme/theme.module';

import { VerticalLayoutComponent } from '@core/layouts/vertical/layout/vertical-layout.component';

@NgModule({
  imports: [
    SharedModule,
    ThemeModule
  ],
  exports: [],
  declarations: []
})
export class VerticalLayoutModule { }
