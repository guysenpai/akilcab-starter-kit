import { takeUntil } from 'rxjs/operators';
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

import { ConfigService, Settings } from '@theme/services/config';
import { AuthenticationService } from '@core/services/authentication/authentication.service';
import { Application } from '@core/data/application';
import { User } from '@core/data/users/user';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'vertical-layout',
  templateUrl: './vertical-layout.component.html',
  styleUrls: ['./vertical-layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VerticalLayoutComponent implements OnInit, OnDestroy {

  settings: Settings;
  user: User;
  company: any;
  apps: Application[];

  private _unsubscribeAll = new Subject();

  constructor(private _configService: ConfigService,
              private _authenticationService: AuthenticationService) { }

  ngOnInit() {
    this._configService.settings.pipe(takeUntil(this._unsubscribeAll)).subscribe((settings: Settings) => {
      this.settings = settings;
    });

    this._authenticationService.onSessionChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe(session => {
      if (session) {
        this.user = session.user;
        this.apps = session.applications;
        this.company = session.company.is_cabinet ? session.company.accounting_firm : session.company;
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
