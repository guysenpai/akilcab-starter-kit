import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';
import { ThemeModule } from '@theme/theme.module';

import { ContentComponent } from '@core/layouts/components/content/content.component';
import { FooterComponent } from '@core/layouts/components/footer/footer.component';
import { NavbarComponent } from '@core/layouts/components/navbar/navbar.component';
import { QuickPanelComponent } from '@core/layouts/components/quick-panel/quick-panel.component';
import { NotificationsTabComponent } from '@core/layouts/components/quick-panel/notifications/notifications-tab.component';
import { OverviewTabComponent } from '@core/layouts/components/quick-panel/overview-tab/overview-tab.component';
import { ToolbarComponent } from '@core/layouts/components/toolbar/toolbar.component';
import { VerticalLayoutComponent } from '@core/layouts/vertical/layout/vertical-layout.component';

@NgModule({
  imports: [
    SharedModule,
    ThemeModule
  ],
  exports: [
    VerticalLayoutComponent
  ],
  declarations: [
    ContentComponent,
    FooterComponent,
    NavbarComponent,
    ToolbarComponent,
    QuickPanelComponent,
    OverviewTabComponent,
    NotificationsTabComponent,
    VerticalLayoutComponent
  ]
})
export class LayoutsModule { }
