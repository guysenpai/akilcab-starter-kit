import { TestBed, inject } from '@angular/core/testing';

import { VerticalLayoutComponent } from '@core/layouts/vertical/layout/vertical-layout.component';
import { AuthenticationGuard } from '@core/services/authentication/authentication.guard';
import { MockAuthenticationService } from '@core/services/authentication/authentication.service.mock';
import { AuthenticationService } from '@core/services/authentication/authentication.service';
import { RouteService } from '@core/services/route/route.service';

describe('RouteService', () => {
  let route: RouteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthenticationGuard,
        { provide: AuthenticationService, useClass: MockAuthenticationService },
        RouteService
      ]
    });
  });

  beforeEach(inject([RouteService], (_route: RouteService) => {
    route = _route;
  }));

  describe('withShell', () => {
    it('should create routes as children of shell', () => {
      // Prepare
      const testRoutes = [{ path: 'test' }];

      // Act
      const result = RouteService.withLayout(testRoutes);

      // Assert
      expect(result.path).toBe('');
      expect(result.children).toBe(testRoutes);
      expect(result.component).toBe(VerticalLayoutComponent);
    });
  });
});
