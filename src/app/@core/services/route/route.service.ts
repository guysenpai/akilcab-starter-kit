import { Route as ngRoute, Routes } from '@angular/router';

import { VerticalLayoutComponent } from '@core/layouts/vertical/layout/vertical-layout.component';
import { AuthenticationGuard } from '@core/services/authentication/authentication.guard';

/**
 * Provides helper methods to create routes.
 */
export class RouteService {

  /**
   * Creates routes using the theme layout component.
   * @param routes The routes to add.
   * @return The new route using shell as the base.
   */
  static withLayout(routes: Routes): ngRoute {
    return {
      path: '',
      component: VerticalLayoutComponent,
      children: routes,
      canActivate: [AuthenticationGuard],
      // Reuse MainLayoutComponent instance when navigating between child views
      data: { reuse: true }
    };
  }

}
