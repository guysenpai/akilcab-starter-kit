import { Injectable } from '@angular/core';
import { CrossStorageClient } from 'cross-storage';
import { Observable, Subscriber, from } from 'rxjs';

import { environment } from '@env/environment';
import { ObservableMediaProvider } from '@angular/flex-layout';

@Injectable({
  providedIn: 'root'
})
export class CrossDomainClientService {

  private storageClient: CrossStorageClient;

  constructor() {
    this.storageClient = new CrossStorageClient(environment.akilCabHubUrl, {
      frameId: 'cross-domain-storage',
      timeout: 10000
    });
  }

  /**
   * Create a guest and connect to the host.
   * @memberof CrossDomainStorageService
   */
  connect(): Observable<any> {
    return from(this.storageClient.onConnect());
  }

  /**
   * Get key from host local storage.
   * @param {string} key Localstorage key.
   * @returns {Observable<any>}
   * @memberof CrossDomainClientService
   */
  get(...keys: string[]): Observable<any> {
    return from(this.storageClient.get(...keys));
  }

  /**
   * Set value to host local storage.
   * @param {string} key Local storage key.
   * @param {string} value Value to set.
   * @returns {Observable<any>}
   * @memberof CrossDomainClientService
   */
  set(key: string, value: string): Observable<any> {
    return from(this.storageClient.set(key, value));
  }

  /**
   * Remove key from  host local storage.
   * @param {string} key Local storage key.
   * @returns {Observable<any>}
   * @memberof CrossDomainClientService
   */
  remove(...keys: string[]): Observable<any> {
    return from(this.storageClient.del(...keys));
  }

  /**
   * Close opened connection.
   * @returns
   * @memberof CrossDomainClientService
   */
  close() {
    return this.storageClient.close();
  }

}
