import { Injectable } from '@angular/core';
import { CrossStorageHub, SubDomain } from 'cross-storage';

@Injectable({
  providedIn: 'root'
})
export class CrossDomainHubService {

  constructor() {}

  /**
   * Create array of allowed domains.
   * @param domains array of allowed domains.
   * @memberof CrossDomainHubService
   */
  init(domains: SubDomain[]) {
    CrossStorageHub.init(domains);
  }

}
