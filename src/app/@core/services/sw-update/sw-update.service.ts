import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { SwUpdate } from '@angular/service-worker';
import { interval } from 'rxjs';

import { Logger } from '@core/services/logger/logger.service';

const log = new Logger('SwUpdateService');

@Injectable({
  providedIn: 'root'
})
export class SwUpdateService {

  constructor(private swUpdate: SwUpdate, private snackbar: MatSnackBar) {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(event => {
        log.debug('current version is', event.current);
        log.debug('available version is', event.available);
        const snack = this.snackbar.open('Update available', 'Reload');

        snack
          .onAction()
          .subscribe(() => this.swUpdate.activateUpdate());
      }, error => log.error(error));

      this.swUpdate.activated.subscribe(event => {
        log.debug('old version was', event.previous);
        log.debug('new version is', event.current);
        window.location.reload(true);
      }, error => log.error(error));

      this.checkForUpdate();
    }
  }

  checkForUpdate() {
    interval(1 * 1 * 60).subscribe(() => this.swUpdate.checkForUpdate());
  }

}
