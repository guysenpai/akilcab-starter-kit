import { TestBed, inject } from '@angular/core/testing';

import { SvgIconService } from './svg-icon.service';

describe('SvgIconService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SvgIconService]
    });
  });

  it('should be created', inject([SvgIconService], (service: SvgIconService) => {
    expect(service).toBeTruthy();
  }));
});
