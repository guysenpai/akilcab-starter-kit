import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SvgIconService {

  constructor(private _domSanitizer: DomSanitizer,
              private _matIconRegistry: MatIconRegistry) {
    this._init();
  }


  private _init() {
    this._matIconRegistry.addSvgIcon('tickets',
      this._domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/tickets.svg'));
    this._matIconRegistry.addSvgIcon('config',
      this._domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/configurations.svg'));
  }
}
