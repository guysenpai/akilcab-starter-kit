import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

export const JWT_OPTIONS = new InjectionToken<JwtOptions>('JWT_OPTIONS');

export interface JwtOptions {
  tokenGetter: () => string | null | Promise<string|null> | Observable<string|null>;
  headerName: string;
  authScheme: string;
  whitelistedDomains: Array<string|RegExp>;
  blacklistedRoutes: Array<string|RegExp>;
  throwNoTokenError: boolean;
  skipWhenExpired: boolean;
}
