import { TestBed, inject } from '@angular/core/testing';

import { JwtTokenInterceptor } from '@core/services/http/interceptors/jwt-token.interceptor';

describe('JwtTokenInterceptor', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JwtTokenInterceptor]
    });
  });

  it('should be created', inject([JwtTokenInterceptor], (interceptor: JwtTokenInterceptor) => {
    expect(interceptor).toBeTruthy();
  }));
});
