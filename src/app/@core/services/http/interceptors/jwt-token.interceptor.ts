import { Injectable, Inject } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { isTokenExpired } from 'jwt-inspect/jwt-helper';
import { Observable, from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { JWT_OPTIONS, JwtOptions } from '@core/services/jwt-options.token';

@Injectable({
  providedIn: 'root'
})
export class JwtTokenInterceptor implements HttpInterceptor {

  private readonly tokenGetter: () => string | null | Promise<string|null> | Observable<string|null>;
  private readonly headerName: string;
  private readonly authScheme: string;
  private readonly whitelistedDomains: Array<string|RegExp>;
  private readonly blacklistedRoutes: Array<string|RegExp>;
  private readonly throwNoTokenError: boolean;
  private readonly skipWhenExpired: boolean;

  constructor(@Inject(JWT_OPTIONS) config: JwtOptions) {
    this.tokenGetter = config.tokenGetter || null;
    this.headerName = config.headerName || 'Authorization';
    this.authScheme = config.authScheme || config.authScheme === '' ? config.authScheme : 'Bearer ';
    this.whitelistedDomains = config.whitelistedDomains || [];
    this.blacklistedRoutes = config.blacklistedRoutes || [];
    this.throwNoTokenError = config.throwNoTokenError || false;
    this.skipWhenExpired = config.skipWhenExpired;
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Do not do anything to requests that do not need Jwt tokens injecting into them.
    if (!this.shouldAttemptTokenInjection(request)) {
      return next.handle(request);
    }

    const token = this.tokenGetter();

    if (token instanceof Promise) {
      return from(token).pipe(mergeMap(
        (asyncToken: string|null) => {
          return this.handleInterception(asyncToken, request, next);
        }
      ));
    } else if (token instanceof Observable) {
      return token.pipe(mergeMap(
        (asyncToken: string|null) => {
          return this.handleInterception(asyncToken, request, next);
        }
      ));
    } else if (token) {
      return this.handleInterception(token, request, next);
    }

    return next.handle(request);
  }

  private isWhitelistedDomain(request: HttpRequest<any>): boolean {
    try {
      const requestUrl = new URL(request.url);
      return this.urlMatchesDomain(requestUrl, this.whitelistedDomains);
    } catch (error) {
      return true;
    }
  }

  private isBlacklistedRoute(request: HttpRequest<any>): boolean {
    try {
      const requestUrl = request.url;
      return this.urlMatchesDomain(requestUrl, this.blacklistedRoutes);
    } catch (error) {
      return false;
    }
  }

  private urlMatchesDomain(url: URL|string, domains: Array<string|RegExp>): boolean {
    return domains.findIndex(domain => {
      if (typeof domain === 'string') {
        return url instanceof URL ? (domain === url.host || domain === url.origin) : domain === url;
      } else if (domain instanceof RegExp) {
        return url instanceof URL ? domain.test(url.host) : domain.test(url);
      } else {
        return false;
      }
    }) > -1;
  }

  private handleInterception(token: string|null, request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!token && this.throwNoTokenError) {
      throw new Error('Could not get token from tokenGetter function');
    }

    if (!this.shouldSkipTokenInjection(token)) {
      request = request.clone({
        setHeaders: {
          [this.headerName]: `${this.authScheme} ${token}`
        }
      });
    }

    return next.handle(request);
  }

  private shouldAttemptTokenInjection(request: HttpRequest<any>): boolean {
    if (this.isBlacklistedRoute(request)) {
      return false;
    }

    if (this.whitelistedDomains.length > 0 && !this.isWhitelistedDomain(request)) {
      return false;
    }
    return true;
  }

  private shouldSkipTokenInjection(token: string): boolean {
    if (!token) {
      return true;
    }

    if (this.skipWhenExpired) {
      return isTokenExpired(token);
    }

    return false;
  }

}
