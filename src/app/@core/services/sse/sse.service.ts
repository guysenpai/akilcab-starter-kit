import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { EventSourcePolyfill, EventSourceInit } from 'ng-event-source';

import { extract } from '@core/services/i18n/i18n.service';
import { Logger } from '@core/services/logger/logger.service';

const log = new Logger('SseService');

@Injectable({
  providedIn: 'root'
})
export class SseService {

  eventSource: EventSourcePolyfill;

  constructor(private _ngZone: NgZone) { }

  createEventSource(url: string, params: EventSourceInit): EventSourcePolyfill {
    return new EventSourcePolyfill(url, params);
  }

  getStream(url: string, params: EventSourceInit): Observable<any> {
    return new Observable((observer) => {
      params = {...params};
      this.eventSource = this.createEventSource(url, params);

      this.eventSource.addEventListener('open', () => {
        log.debug(`Connecté au flux.`);
      });

      this.eventSource.addEventListener('error', (event: any) => {
        if (event.target.readyState === this.eventSource.CLOSED) {
          log.debug(extract(`Le flux a été fermé par le serveur.`));
          this.closeStream();
          observer.complete();
        } else if (event.target.readyState === this.eventSource.CONNECTING) {
          log.debug(`Connection au flux...`);
        } else {
          observer.error(`EventSource error: ${event}`);
        }
      });

      this.eventSource.addEventListener('message', (event: any) => {
        this._ngZone.run(() => observer.next(JSON.parse(event.data)));
      });
    });
  }

  closeStream() {
    this.eventSource.close();
  }
}
