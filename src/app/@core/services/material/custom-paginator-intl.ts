import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material';

import { extract } from '@core/services/i18n/i18n.service';

@Injectable()
export class CustomPaginatorIntl extends MatPaginatorIntl {

  constructor() {
    super();

    this.itemsPerPageLabel = extract('Éléments par page');
    this.nextPageLabel = extract('Suivant');
    this.previousPageLabel = extract('Précédant');
    this.firstPageLabel = extract('Premier');
    this.lastPageLabel = extract('Dernier');
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return extract('0 de ') + length;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return startIndex + 1 + ' - ' + endIndex + extract(' de ') + length;
  }

}
