import { DOCUMENT } from '@angular/common';
import { Injectable, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Logger } from '@core/services/logger/logger.service';
import { AuthenticationService } from '@core/services/authentication/authentication.service';

const log = new Logger('AuthenticationGuard');

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

  constructor(private _router: Router,
              private _matSnackbar: MatSnackBar,
              @Inject(DOCUMENT) private _document: any,
              private _authenticationService: AuthenticationService) { }

  canActivate(
    next?: ActivatedRouteSnapshot,
    state?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._authenticationService.isAuthenticated()) {
      const session = this._authenticationService.credentials;
      if (session && session.profile) {
        switch (session.profile.profil_type) {
          case 'PROFILE_ACCOUNTINGFIRM':
            this._authorize(state, 'cabinet');
            return true;
          case 'PROFILE_CUSTOMER':
            this._authorize(state, 'cabinet');
            return true;
          default:
            log.debug('Not authorized');
            this._matSnackbar.open(`Vous n'êtes pas autorisé à accéder à cette page.`, null, {
              horizontalPosition: 'end',
              panelClass: 'warn-bg'
            });
            return false;
        }
      }
    }

    log.debug('Not authenticated, redirecting...');
    this._matSnackbar.open(`Échec d'authentification! Vous serez redirigé...`, null, { panelClass: 'red-bg' });
    this._document.location.href = this._authenticationService.redirectUrl;
    return false;
  }

  private _authorize(state: RouterStateSnapshot, role: string): Observable<boolean> | Promise<boolean> | boolean {
    return state.url.startsWith(`/app/${role}`) ? true : this._router.navigateByUrl(`/app/${role}`, { replaceUrl: true });
  }
}
