import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isTokenExpired } from 'jwt-inspect/jwt-helper';
import { Observable, BehaviorSubject, of, throwError } from 'rxjs';
import { map, catchError, tap, switchMap } from 'rxjs/operators';

import { Credentials } from '@core/services/authentication/interfaces/credentials';
import { LoginContext } from '@core/services/authentication/interfaces/login-context';
import { CrossDomainClientService } from '@core/services/cross-domain-storage/cross-domain-client.service';

import { environment } from '@env/environment';

const credentialsKey = environment.credentialsKey;

/**
 * Provides a base for authentication workflow.
 * The Credentials interface as well as login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  redirectUrl = `${environment.akilCabHubUrl}/#/auth/logout?return_to=${environment.redirectBackUrl}`;
  onSessionChanged = new BehaviorSubject<Credentials>(null);

  /**
   * Gets the user credentials.
   * @return The user credentials or null if the user is not authenticated.
   */
  get credentials(): Credentials | null {
    return this._credentials;
  }

  private _credentials: Credentials | null;

  constructor(private _http: HttpClient,
              private _crossDomainClientService: CrossDomainClientService) {
    const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
      this.onSessionChanged.next(this.credentials);
    }
  }

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
    const data = {
      username: context.login,
      password: context.password
    };

    return of({
      user: {
        avatar_url: 'assets/images/avatars/profile.jpg',
        civility: 'M.',
        first_name: 'Guy',
        last_name: 'Senpai',
        username: 'guysenpai'
      },
      activeProfile: {
        physical_person: false,
        first_name: 'Guy',
        last_name: 'Senpai',
        social_reason: 'AfroGeekLand Dev Studio'
      },
      token: 'abcdefghijklmnopqrstuvwxyz0123456789'
    });
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    this._setCredentials();

    return this._crossDomainClientService.connect().pipe(
      switchMap(() => this._crossDomainClientService.remove('akilCabCredentials')),
      map(() => true),
      catchError(error => throwError(error))
    );
  }

  /**
   * Checks is the user is authenticated.
   * @return True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials && !isTokenExpired(this.credentials.token);
  }

  /**
   * Gets Cross Storage credentials and checks token expiration.
   */
  checkCrossStorageCredentials(): Observable<boolean> {
    return this.getCrossStorageCredentials().pipe(
      map(data => {
        if (data && data.credentials && !isTokenExpired(data.credentials.token)) {
          this._setLocalCredentials(data);
          return true;
        } else {
          this._setCredentials();
          return false;
        }
      })
    );
  }

  /**
   * Gets Cross Storage credentials.
   */
  getCrossStorageCredentials(): Observable<any> {
    return this._crossDomainClientService.connect().pipe(
      switchMap(() => this._crossDomainClientService.get('akilCabCredentials')),
      map(response => JSON.parse(response)),
      catchError(error => throwError(error))
    );
  }

  /**
   * Gets Cross Storage Credentials to set in current local/session storage
   * @param data Cross Storage credentials
   */
  private _setLocalCredentials(data: any) {
    if (data && data.credentials) {
      const credentials = data.credentials;
      const isCabinet = credentials.activeStructure && credentials.activeStructure.is_cabinet;
      let profile;

      if (credentials.user && credentials.user.pro) {
        if (isCabinet) {
          if (credentials.user.profiles.length === 1) {
            profile = credentials.user.profiles[0];
          } else {}
        } else {
          if (credentials.user.profiles.length === 1) {
            profile = credentials.user.profiles[0];
          } else {}
        }
      }

      const localCredentials = {
        user: credentials.user,
        token: credentials.token,
        company: credentials.activeStructure || null,
        profile: profile || credentials.user.profiles[0],
        applications: credentials.available_applications || [],
      };
      this._setCredentials(localCredentials);
    }
  }

  /**
   * Sets the user credentials.
   * The credentials may be persisted across sessions by setting the `remember` parameter to true.
   * Otherwise, the credentials are only persisted for the current session.
   * @param credentials The user credentials.
   * @param remember True to remember credentials across sessions.
   */
  private _setCredentials(credentials?: Credentials, remember?: boolean) {
    this._credentials = credentials || null;

    if (credentials) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(credentialsKey, JSON.stringify(credentials));
      this.onSessionChanged.next(this.credentials);
    } else {
      sessionStorage.removeItem(credentialsKey);
      localStorage.removeItem(credentialsKey);
      this.onSessionChanged.next(null);
    }
  }

}
