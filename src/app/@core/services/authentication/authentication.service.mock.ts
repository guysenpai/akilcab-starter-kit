import { Observable, of } from 'rxjs';

import { Credentials } from '@core/services/authentication/interfaces/credentials';
import { LoginContext } from '@core/services/authentication/interfaces/login-context';

export class MockAuthenticationService {

  credentials: Credentials | null = {
    user: {},
    token: '123'
  };

  login(context: LoginContext): Observable<Credentials> {
    return of({
      user: {id: 1, username: context.login},
      token: '123456'
    });
  }

  logout(): Observable<boolean> {
    this.credentials = null;
    return of(true);
  }

  isAuthenticated(): boolean {
    return !!this.credentials;
  }
}
