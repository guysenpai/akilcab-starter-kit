export interface LoginContext {
  login: string;
  password: string;
  remember?: boolean;
}
