import { User } from '@core/data/users/user';
import { UserCompany } from '@core/data/users/user-company';
import { UserProfile } from '@core/data/users/user-profile';
import { Application } from '@core/data/application';

export interface Credentials {
  user: User;
  company?: UserCompany;
  profile?: UserProfile;
  structures?: any[];
  applications?: Application[];
  token: string;
}
