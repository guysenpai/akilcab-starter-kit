import { CommonModule } from '@angular/common';
import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatPaginatorIntl } from '@angular/material';
import { RouterModule, RouteReuseStrategy } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';

import { SharedModule } from '@shared/shared.module';

import { HttpService } from '@core/services/http/http.service';
import { RouteReusableStrategy } from '@core/services/route/route-reusable-strategy';
import { CustomPaginatorIntl } from '@core/services/material/custom-paginator-intl';
import { JWT_OPTIONS, JwtOptions } from '@core/services/jwt-options.token';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    TranslateModule.forRoot(),
    SharedModule,
  ],
  declarations: [],
  providers: [
    CookieService,
    { provide: HttpClient, useClass: HttpService },
    { provide: MatPaginatorIntl, useClass: CustomPaginatorIntl },
    { provide: RouteReuseStrategy, useClass: RouteReusableStrategy },
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        duration: 2500,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      }
    }
  ]
})
export class CoreModule {

  static forRoot(options: JwtOptions): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        { provide: JWT_OPTIONS, useValue: options }
      ]
    };
  }

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }

}
