import { DOCUMENT } from '@angular/common';
import { Injectable, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthenticationService } from '@core/services/authentication/authentication.service';
import { Logger } from '@core/services/logger/logger.service';

const log = new Logger('RoleGuard');

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivateChild {

  constructor(private _router: Router,
              private _matSnackbar: MatSnackBar,
              @Inject(DOCUMENT) private _document: any,
              private _authenticationService: AuthenticationService) { }

  canActivateChild(
    childRoute?: ActivatedRouteSnapshot,
    state?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._authenticationService.isAuthenticated()) {
      this._authenticationService.onSessionChanged.subscribe(session => {
        if (session && session.profile) {
          switch (session.profile.profil_type) {
            case 'PROFILE_ACCOUNTINGFIRM':
              return this._authorize(state, 'cabinet');
            case 'PROFILE_CUSTOMER':
              return this._authorize(state, 'client');
            default:
              log.debug('Not authorized');
              this._matSnackbar.open(`Vous n'êtes pas autorisé à accéder à cette page.`, null, {
                horizontalPosition: 'end',
                panelClass: 'warn-bg'
              });
              return false;
          }
        }
      });
      return true;
    }

    log.debug('Not authenticated, redirecting...');
    this._matSnackbar.open(`Échec d'authentification! Vous serez redirigé...`, null, { panelClass: 'red-bg' });
    this._document.location.href = this._authenticationService.redirectUrl;
    return false;
  }

  private _authorize(state: RouterStateSnapshot, role: string): Observable<boolean> | Promise<boolean> | boolean {
    return state.url.startsWith(`/app/${role}`) ? true : this._router.navigateByUrl(`/app/${role}`, { replaceUrl: true });
  }
}
