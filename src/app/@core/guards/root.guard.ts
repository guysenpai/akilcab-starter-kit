import { DOCUMENT } from '@angular/common';
import { Injectable, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { AuthenticationService } from '@core/services/authentication/authentication.service';
import { Logger } from '@core/services/logger/logger.service';

const log = new Logger('RootGuard');

@Injectable({
  providedIn: 'root'
})
export class RootGuard implements CanActivate {

  constructor(private _matSnackbar: MatSnackBar,
              @Inject(DOCUMENT) private _document: any,
              private _authenticationService: AuthenticationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this._authenticationService.checkCrossStorageCredentials().pipe(
      tap(authenticated => authenticated ? true : this._redirectToLogin()),
      catchError(error => {
        log.debug('Cross Storage Error: ', error);
        return of(this._authenticationService.isAuthenticated());
      }),
      map(authenticated => authenticated ? true : this._redirectToLogin())
    );
  }

  private _redirectToLogin() {
    this._matSnackbar.open(`Échec d'authentification! Vous serez redirigé...`, null, { panelClass: 'red-bg' });
    this._document.location.href = this._authenticationService.redirectUrl;
    return false;
  }
}
