import { NgModule } from '@angular/core';

import { ThemeModule } from '@theme/theme.module';
import { SharedModule } from '@shared/shared.module';

import { ClientDashboardRoutingModule } from './client-dashboard-routing.module';
import { ClientDashboardComponent } from './client-dashboard.component';

@NgModule({
  imports: [
    SharedModule,
    ThemeModule,
    ClientDashboardRoutingModule
  ],
  declarations: [ClientDashboardComponent]
})
export class ClientDashboardModule { }
