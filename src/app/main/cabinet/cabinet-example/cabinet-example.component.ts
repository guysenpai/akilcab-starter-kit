import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { fromEvent, Subject } from 'rxjs';
import { tap, debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { themeAnimations } from '@theme/animations';
import { Example } from '@app/@core/data/example/example';
import { ExampleService } from '@core/data/example/example.service';
import { ExampleDataSource } from '@core/data/example/example.datasource';

@Component({
  selector: 'akilcab-cabinet-example',
  templateUrl: './cabinet-example.component.html',
  styleUrls: ['./cabinet-example.component.scss'],
  animations: [...themeAnimations]
})
export class CabinetExampleComponent implements OnInit, OnDestroy {

  @ViewChild('filter') filter: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  hideFilterClose = true;
  displayedColumns = ['picture', 'code', 'company_name', 'rc', 'referential', 'actions'];
  tableDataSource: ExampleDataSource;

  isLoading: boolean;
  emptyDataMessage = 'Aucune donnée disponible.';

  private _unsubscribeAll = new Subject();

  constructor(private _exampleService: ExampleService) { }

  ngOnInit() {
    this.isLoading = true;

    this._exampleService.getExampleData()
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(() => {
          setTimeout(() => this.isLoading = false);
          this.tableDataSource = new ExampleDataSource(this._exampleService, this.paginator, this.sort);
        }, () => {
          setTimeout(() => this.isLoading = false);
          this.tableDataSource = new ExampleDataSource(this._exampleService, this.paginator, this.sort);
        });

    fromEvent(this.filter.nativeElement, 'keyup').pipe(
      tap((event: any) => this.hideFilterClose = event.target.value ? false : true),
      takeUntil(this._unsubscribeAll),
      debounceTime(150),
      distinctUntilChanged()
    ).subscribe(() => {
      if (this.tableDataSource) {
        this.tableDataSource.filter = this.filter.nativeElement.value;
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  resetFilter() {
    this.filter.nativeElement.value = '';
    this.hideFilterClose = true;
  }

}
