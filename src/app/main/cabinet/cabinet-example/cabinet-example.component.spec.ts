import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinetExampleComponent } from './cabinet-example.component';

describe('CabinetExampleComponent', () => {
  let component: CabinetExampleComponent;
  let fixture: ComponentFixture<CabinetExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinetExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinetExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
