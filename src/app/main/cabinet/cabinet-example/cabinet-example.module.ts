import { NgModule } from '@angular/core';

import { ThemeModule } from '@theme/theme.module';
import { SharedModule } from '@shared/shared.module';

import { CabinetExampleRoutingModule } from './cabinet-example-routing.module';
import { CabinetExampleComponent } from './cabinet-example.component';

@NgModule({
  imports: [
    SharedModule,
    ThemeModule,
    CabinetExampleRoutingModule
  ],
  declarations: [CabinetExampleComponent]
})
export class CabinetExampleModule { }
