import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CabinetExampleComponent } from './cabinet-example.component';

const routes: Routes = [
  { path: '', component: CabinetExampleComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CabinetExampleRoutingModule { }
