import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CabinetRoutingModule } from './cabinet-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CabinetRoutingModule
  ],
  declarations: []
})
export class CabinetModule { }
