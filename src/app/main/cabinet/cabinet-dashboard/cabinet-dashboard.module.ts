import { NgModule } from '@angular/core';

import { ThemeModule } from '@theme/theme.module';
import { SharedModule } from '@shared/shared.module';

import { CabinetDashboardRoutingModule } from './cabinet-dashboard-routing.module';
import { CabinetDashboardComponent } from './cabinet-dashboard.component';

@NgModule({
  imports: [
    SharedModule,
    ThemeModule,
    CabinetDashboardRoutingModule
  ],
  declarations: [CabinetDashboardComponent]
})
export class CabinetDashboardModule { }
