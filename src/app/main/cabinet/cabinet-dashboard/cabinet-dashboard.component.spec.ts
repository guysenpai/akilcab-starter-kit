import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinetDashboardComponent } from './cabinet-dashboard.component';

describe('CabinetDashboardComponent', () => {
  let component: CabinetDashboardComponent;
  let fixture: ComponentFixture<CabinetDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinetDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinetDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
