import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CabinetDashboardComponent } from './cabinet-dashboard.component';

const routes: Routes = [
  { path: '', component: CabinetDashboardComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CabinetDashboardRoutingModule { }
