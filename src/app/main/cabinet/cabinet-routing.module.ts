import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', loadChildren: './cabinet-dashboard/cabinet-dashboard.module#CabinetDashboardModule' },
  { path: 'example', loadChildren: './cabinet-example/cabinet-example.module#CabinetExampleModule' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CabinetRoutingModule { }
