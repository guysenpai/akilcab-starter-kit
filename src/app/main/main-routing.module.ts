import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationGuard } from '@core/services/authentication/authentication.guard';
import { ProfileGuard } from '@core/guards/profile.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthenticationGuard],
    canActivateChild: [ProfileGuard],
    children: [
      { path: 'cabinet', loadChildren: './cabinet/cabinet.module#CabinetModule' },
      { path: 'client', loadChildren: './client/client.module#ClientModule' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
