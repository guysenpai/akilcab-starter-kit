import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RootGuard } from '@core/guards/root.guard';
import { ProfileGuard } from '@core/guards/profile.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [RootGuard],
    children: [
      { path: '', redirectTo: 'app', pathMatch: 'full' },
      { path: 'app', loadChildren: './main/main.module#MainModule' },
    ]
  },
  { path: '', redirectTo: 'app', pathMatch: 'full' },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
