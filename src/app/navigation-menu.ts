import { extract } from '@core/services/i18n/i18n.service';
import { Navigation } from '@theme/components/navigation/navigation';

export const cabinetNavigationMenu: Navigation = [
  {
    title: '',
    type: 'group',
    children: [
      {
        title: extract('Tableau de bord'),
        translate: 'Tableau de bord',
        type: 'item',
        icon: 'dashboard',
        url: '/app/cabinet/dashboard'
      }, {
        title: extract('Mes Clients'),
        translate: 'Mes Clients',
        type: 'item',
        icon: 'folder_shared',
        url: '/app/cabinet/dossiers-clients'
      }, {
        title: extract('Mes Plans Comptables'),
        translate: 'Mes Plans Comptables',
        type: 'collapse',
        icon: 'assignment',
        url: '/app/cabinet/plans-comptables',
        children: [
          {
            title: extract('Liste des Plans Comptables'),
            translate: 'Liste des Plans Comptables',
            type: 'item',
            url: '/app/cabinet/plans-comptables'
          }, {
            title: extract('Créer un Plan Comptable'),
            translate: 'Créer un Plan Comptable',
            type: 'item',
            url: '/app/cabinet/plans-comptables/creation'
          }, {
            title: extract('Importer un Plan Comptable'),
            translate: 'Importer un Plan Comptable',
            type: 'item',
            url: '/app/cabinet/importation'
          }
        ]
      }, {
        title: extract('Gestion des utilisateurs'),
        translate: 'Gestion des utilisateurs',
        type: 'collapse',
        icon: 'people',
        children: [
          {
            title: extract('Collaborateurs'),
            translate: 'Collaborateurs',
            type: 'item',
            url: '/app/cabinet/utilisateurs/collaborateurs'
          }, {
            title: extract('Comptes Clients'),
            translate: 'Comptes Clients',
            type: 'item',
            url: '/app/cabinet/utilisateurs/comptes-clients'
          }
        ]
      }
    ]
  }
];

export const clientNavigationMenu: Navigation = [
  {
    title: '',
    type: 'group',
    children: [
      {
        title: extract('Tableau de bord'),
        translate: 'Tableau de bord',
        type: 'item',
        icon: 'dashboard',
        url: '/app/client/dashboard'
      }, {
        title: extract('Mes Exercices'),
        translate: 'Mes Exercices',
        type: 'item',
        icon: 'dashboard',
        url: '/app/client/exercices'
      }, {
        title: extract('Mes Collaborateurs'),
        translate: 'Mes Collaborateurs',
        type: 'item',
        icon: 'people',
        url: '/app/client/collaborateurs'
      }, {
        title: extract('E-bot Banking'),
        translate: 'E-bot Banking',
        type: 'item',
        icon: 'bug_report',
        url: '/app/client/e-bot-banking'
      }, {
        title: extract('Mes informations'),
        translate: 'Mes informations',
        type: 'item',
        icon: 'info',
        url: '/app/client/dashboard'
      }, {
        title: extract('Assistance'),
        translate: 'Assistance',
        type: 'item',
        icon: 'local_offer',
        url: '/app/client/dashboard'
      }
    ]
  }
];
