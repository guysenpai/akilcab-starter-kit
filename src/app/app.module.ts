import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';

import { CoreModule } from '@core/core.module';
import { LayoutsModule } from '@core/layouts/layouts.module';
import { SharedModule } from '@shared/shared.module';
import { ThemeModule } from '@theme/theme.module';

import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { appSettings } from '@app/app.settings';

import { environment } from '@env/environment';

export function tokenGetter() {
  const savedCredentials = sessionStorage.getItem(environment.credentialsKey) || localStorage.getItem(environment.credentialsKey);
  return savedCredentials ? JSON.parse(savedCredentials).token : null;
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule.forRoot({
      tokenGetter: tokenGetter,
      headerName: 'Authorization',
      authScheme: 'Bearer',
      whitelistedDomains: [environment.serverUrl],
      blacklistedRoutes: [],
      throwNoTokenError: true,
      skipWhenExpired: false
    }),
    SharedModule,
    ThemeModule.forRoot(appSettings),
    LayoutsModule,
    AppRoutingModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
